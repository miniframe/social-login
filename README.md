# Miniframe Social Login

This package implements social logins to the [Miniframe PHP Framework](https://bitbucket.org/miniframe).

[![build](https://miniframe.dev/build/badge/social-login)](https://bitbucket.org/miniframe/social-login/addon/pipelines/home)
[![code coverage](https://miniframe.dev/codecoverage/badge/social-login)](https://miniframe.dev/codecoverage/social-login)

## How to use

1. In your existing project, type: `composer require miniframe/social-login`
2. Add the directive below to your config.ini

*Example config.ini directives*
```ini
[framework]
base_href = /
middleware[] = Miniframe\Middleware\Session
middleware[] = Miniframe\SocialLogin\Middleware\SocialLogin
```

Also, add a secret string like this into a secrets.ini that should be in your .gitignore file:
```ini
[sociallogin]
state_secret = "********************************"
```

If specific pages shouldn't require a login, you can specify exclusions.
The available wildcards are `*` (one or more characters) and `?` (one character).

```ini
[sociallogin]
exclude[] = /public
exclude[] = /public/*
```

If you want to have a "Close" button to go back to a public page, add this:

```ini
[sociallogin]
; URL for the "Close" button in the upper right corner of the login dialog
public_href = /public
```

If you want to enable dark mode support, you can add:

```ini
[sociallogin]
; darkmode can be "on", "off" or "auto", default is "off"
darkmode = on
```

When just one provider is enabled, you can force autologin by setting:

```ini
[sociallogin]
autologin = true
```

## Supported providers

All OAuth 2.0 ([RFC6749](https://datatracker.ietf.org/doc/html/rfc6749))
and OAuth 1.0 ([RFC5849](https://datatracker.ietf.org/doc/html/rfc5849))
providers should be supported, but this package is tested with:

* [Atlassian](src/Provider/Atlassian.md)
* [Bitbucket](src/Provider/Bitbucket.md)
* [Github](src/Provider/Github.md)
* [Gitlab](src/Provider/Gitlab.md)
* [Microsoft](src/Provider/Microsoft.md)
* [Google](src/Provider/Google.md)
* [Facebook](src/Provider/Facebook.md)
* [Twitter](src/Provider/Twitter.md)
* [Instagram](src/Provider/Instagram.md)
* [Reddit](src/Provider/Reddit.md)
* [Linkedin](src/Provider/Linkedin.md)

Click on the name of a provider to see how you can configure that specific provider.

Besides remote login, there's also an option to log in with just an email addres.
When you fill in your email address, you'll receive an email with a one-time code that works for 10 minutes (or until used) to log in.

For more about the [Email](src/Provider/Email.md) provider see [src/Provider/Email.md](src/Provider/Email.md)

## Create your own provider

To create your own provider, you can use the current providers as example.

The middleware looks for providers in two namespaces: `Miniframe\SocialLogin\Provider` and `App\SocialLogin\Provider`.

## Authentication versus Authorization

This library only handles Authentication (Who are you?). It doesn't check Authorization (what are you allowed to do?)
For that, you'll need to validate the permissions for the specific user in your own application.

## For Windows Developers

In the `bin` folder, a few batch files exist, to make development easier.

If you install [Docker Desktop for Windows](https://www.docker.com/products/docker-desktop),
you can use [bin\composer.bat](bin/composer.bat), [bin\phpcs.bat](bin/phpcs.bat), [bin\phpunit.bat](bin/phpunit.bat), [bin\phpstan.bat](bin/phpstan.bat) and [bin\security-checker.bat](bin\security-checker.bat) as shortcuts for Composer, CodeSniffer, PHPUnit, PHPStan and the Security Checker, without the need of installing PHP and other dependencies on your machine.

The same Docker container and tools are used in [Bitbucket Pipelines](https://bitbucket.org/miniframe/social-login/addon/pipelines/home) to automatically test this project.
