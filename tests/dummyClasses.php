<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\SocialLogin\Model\User;

class Dummy implements ProviderInterface
{
    /**
     * Creates a new provider
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
    }

    /**
     * Starts the authentication process
     *
     * @return User
     */
    public function authenticate(): User
    {
        return new User('foo', 'bar', 'baz', 'foo', 'bar', ['baz']);
    }

    /**
     * Returns the image source for the logo of this provider.
     *
     * @return string
     */
    public static function getLogoSource(): string
    {
        return 'https://www.stefanthoolen.nl/logo.svg';
    }

    /**
     * Returns the theme color for this provider.
     *
     * @return string
     */
    public static function getThemeColor(): string
    {
        return 'white';
    }
}

class DummyOAuth2Provider extends AbstractOAuth2Provider
{
    /**
     * Defines a specific test case
     *
     * Can be one of:
     * - curlGetWithDataArray
     * - curlWithInvalidRequestMethod
     *
     * @var string
     */
    public static $testcase = 'curlGetWithDataArray';

    /**
     * Returns the image source for the logo of this provider.
     *
     * @return string
     */
    public static function getLogoSource(): string
    {
        return 'https://www.stefanthoolen.nl/logo.svg';
    }

    /**
     * Returns the theme color for this provider.
     *
     * @return string
     */
    public static function getThemeColor(): string
    {
        return 'white';
    }

    /**
     * Returns the Authorize URL
     *
     * @return string
     */
    protected function getAuthorizeUrl(): string
    {
        return 'https://foo.bar.baz/authorize?foo=bar';
    }

    /**
     * Returns the Access token URL
     *
     * @return string
     */
    protected function getAccessTokenUrl(): string
    {
        return 'https://foo.bar.baz/accesstoken?foo=bar';
    }

    /**
     * Returns no scope
     *
     * @return string|null
     */
    protected function getScope(): ?string
    {
        return null;
    }

    /**
     * Returns a dummy user
     *
     * @param array $accessToken The access token.
     *
     * @return User
     */
    protected function getUserProfile(array $accessToken): User
    {
        if (static::$testcase == 'curlGetWithDataArray') {
            $this->curlRequest('https://foo.bar.baz/', 'GET', ['foo' => 'bar']);
        }
        if (static::$testcase == 'curlWithInvalidRequestMethod') {
            $this->curlRequest('https://foo.bar.baz/', 'TEAPOT', ['foo' => 'bar']);
        }
        return new User('foo', 'foo@bar.baz', 'Foo Bar', 'https://foo.bar.baz/image.jpg', __CLASS__, []);
    }
}

class Notaprovider
{
}

namespace App\SocialLogin\Provider;

use Miniframe\SocialLogin\Provider\Dummy;

class Customdummyprovider extends Dummy
{
}
