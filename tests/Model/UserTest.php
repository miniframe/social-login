<?php

namespace Miniframe\SocialLogin\Model;

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * Tests if all values are set correctly through the constructor
     *
     * @return void
     */
    public function testConstructor(): void
    {
        $user = new User(
            '123',
            'sthoolen',
            'Stefan Thoolen',
            'https://www.stefanthoolen.nl/avatar.jpg',
            'Miniframe\\SocialLogin\\Provider\\DummyProvider',
            ['foo' => 'bar']
        );

        $this->assertEquals('123', $user->getUniqueKey());
        $this->assertEquals('sthoolen', $user->getUserName());
        $this->assertEquals('Stefan Thoolen', $user->getDisplayName());
        $this->assertEquals('https://www.stefanthoolen.nl/avatar.jpg', $user->getAvatar());
        $this->assertEquals('Miniframe\\SocialLogin\\Provider\\DummyProvider', $user->getProviderFQCN());
        $this->assertEquals(['foo' => 'bar'], $user->getRawData());
    }

    /**
     * Tests if all values are set correctly when using a state
     *
     * @return void
     */
    public function testSetState(): void
    {
        $state = [
            'uniqueKey' => '123',
            'userName' => 'sthoolen',
            'displayName' => 'Stefan Thoolen',
            'avatar' => 'https://www.stefanthoolen.nl/avatar.jpg',
            'providerFQCN' => 'Miniframe\\SocialLogin\\Provider\\DummyProvider',
            'rawData' => ['foo' => 'bar']
        ];

        $user = User::__set_state($state);

        $this->assertEquals('123', $user->getUniqueKey());
        $this->assertEquals('sthoolen', $user->getUserName());
        $this->assertEquals('Stefan Thoolen', $user->getDisplayName());
        $this->assertEquals('https://www.stefanthoolen.nl/avatar.jpg', $user->getAvatar());
        $this->assertEquals('Miniframe\\SocialLogin\\Provider\\DummyProvider', $user->getProviderFQCN());
        $this->assertEquals(['foo' => 'bar'], $user->getRawData());
    }
}
