<?php

namespace Miniframe\SocialLogin\Provider;

/**
 * Mocks the curl_exec() method
 *
 * @param resource $ch A cURL handle returned by curl_init().
 *
 * @return string|boolean Returns true on success or false on failure. However, if the CURLOPT_RETURNTRANSFER option is
 *                        set, it will return the result on success, false on failure.
 */
function curl_exec($ch)
{
    $curlInfo = \curl_getinfo($ch);
    $return = $GLOBALS['CurlExecMock']($ch, $curlInfo);
    $GLOBALS['CurlInfoMock'] = $curlInfo;
    return $return;
}

/**
 * Mocks the curl_getinfo() method
 *
 * @param resource     $ch     A cURL handle returned by curl_init().
 * @param integer|null $option Option constant.
 *
 * @return mixed
 */
function curl_getinfo($ch, ?int $option = null)
{
    if ($option !== null) {
        throw new \InvalidArgumentException('Can\'t mock $option parameter');
    }

    return $GLOBALS['CurlInfoMock'];
}

/**
 * Defines a mock method to validate a cURL request
 *
 * @param callable $validateMethod The callable.
 *
 * @return void
 */
function setCurlExecMock(callable $validateMethod): void
{
    $GLOBALS['CurlExecMock'] = $validateMethod;
}
