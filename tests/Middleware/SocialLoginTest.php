<?php

namespace Miniframe\SocialLogin\Middleware;

use Miniframe\SocialLogin\Controller\Login as LoginController;
use Miniframe\SocialLogin\Controller\Email as EmailController;
use Miniframe\Core\Config;
use Miniframe\Core\Registry;
use Miniframe\Core\Request;
use Miniframe\Middleware\Session;
use Miniframe\Response\RedirectResponse;
use Miniframe\SocialLogin\Model\User;
use Miniframe\SocialLogin\Provider\Email;
use PHPUnit\Framework\TestCase;

class SocialLoginTest extends TestCase
{
    /**
     * This method is called before each test.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // Set the state secret
        $reflection = new \ReflectionProperty(SocialLogin::class, 'stateSecret');
        $reflection->setAccessible(true);
        $reflection->setValue(null, 'foobar');
    }

    /**
     * Clears session at teardown
     *
     * @return void
     */
    protected function tearDown(): void
    {
        if (Registry::has(Session::class)) {
            Registry::get(Session::class)->commit();
        }
        Registry::register(Session::class, null);
    }

    /**
     * Dataprovider for testUrlFilter
     *
     * @return array[]
     */
    public function urlDataProvider(): array
    {
        return [
            'Root URL' => ['/', false],
            'Excluded full match' => ['/public', true],
            'Excluded by wildcard' => ['/public/test', true],
            'Excluded because login page' => ['/_SOCIALLOGIN/login', true],
            'Protected URL' => ['/foo/bar', false],
        ];
    }

    /**
     * Dataprovider for testRouting
     *
     * @return array[][]
     */
    public function routerDataProvider(): array
    {
        return [
            ['url' => '/public', 'result' => null],
            ['url' => '/_SOCIALLOGIN/login', 'result' => [LoginController::class, 'login']],
            ['url' => '/_SOCIALLOGIN/404', 'result' => null],
            ['url' => '/_SOCIALLOGIN/email/404', 'result' => null],
            ['url' => '/_SOCIALLOGIN/email/code', 'result' => [EmailController::class, 'code']],
            ['url' => '/_SOCIALLOGIN/login/dummy', 'result' => [LoginController::class, 'login']],
        ];
    }

    /**
     * Test the constructor with no session defined
     *
     * @return void
     */
    public function testNoSessions(): void
    {
        $request = new Request(['REQUEST_URI' => '/']);
        $config = Config::__set_state(['configFolder' => __DIR__, 'projectFolder' => __DIR__, 'data' => [
            'sociallogin' => ['providers' => ['Email'], 'state_secret' => 'foobar']
        ]]);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessageMatches('/^The SocialLogin extension requires the Session middleware/');
        new SocialLogin($request, $config);
    }

    /**
     * Test the constructor with an invalid provider config
     *
     * @return void
     */
    public function testInvalidProviderConfig(): void
    {
        $request = new Request(['REQUEST_URI' => '/']);
        $config = Config::__set_state(['configFolder' => __DIR__, 'projectFolder' => __DIR__, 'data' => [
            'sociallogin' => ['providers' => 'Email', 'state_secret' => 'foobar']
        ]]);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessageMatches('/^The providers config value should be a list./');
        new SocialLogin($request, $config);
    }

    /**
     * Tests if urls are allowed
     *
     * @param string  $url     The URL to test.
     * @param boolean $allowed Whether or not it should be allowed.
     *
     * @dataProvider urlDataProvider
     * @return       void
     */
    public function testUrlFilter(string $url, bool $allowed): void
    {
        if (!$allowed) {
            $this->expectException(RedirectResponse::class);
        }
        mockRequest($url, null, ['Email' => []], [], $request, $config, $session, $socialLogin);
        $this->assertTrue($allowed);
    }

    /**
     * When logged in, all URLs should work.
     *
     * @param string $url The URL to test.
     *
     * @dataProvider urlDataProvider
     * @return       void
     */
    public function testLoggedin(string $url): void
    {
        $data['_SOCIALLOGIN']['loggedin'] = true;
        mockRequest($url, null, ['Email' => []], $data, $request, $config, $session, $socialLogin);
        $this->assertTrue(true);
    }

    /**
     * Tests the login, getUser and logout methods
     *
     * @return void
     */
    public function testLoginGetUserLogout(): void
    {
        $user = new User(
            'foo',
            'bar',
            'baz',
            'https://foo.bar.baz/',
            Email::class,
            ['foo' => 'bar']
        );

        mockRequest('/_SOCIALLOGIN/login', null, ['Email' => []], [], $request, $config, $session, $socialLogin);
        $socialLogin->login($user);
        $this->assertEquals($user, $socialLogin->getUser());
        $socialLogin->logout();
        $this->assertEquals(null, $socialLogin->getUser());
    }

    /**
     * Tests the getLogoutHref method.
     *
     * @return void
     */
    public function testGetLogoutHref(): void
    {
        mockRequest('/public', null, ['Email' => []], [], $request, $config, $session, $socialLogin);
        $state = SocialLogin::generateState(['redirectUrl' => '/public']);
        $this->assertEquals(
            '/_SOCIALLOGIN/logout?state=' . rawurlencode($state),
            $socialLogin->getLogoutHref()
        );
    }

    /**
     * Can we locate a custom provider?
     *
     * @return void
     */
    public function testCustomProvider(): void
    {
        mockRequest('/public', null, ['Customdummyprovider' => []], [], $request, $config, $session, $socialLogin);
        $this->assertTrue(true);
    }

    /**
     * Can we locate a non-existing provider
     *
     * @return void
     */
    public function testNonExistingProvider(): void
    {
        $this->expectException(\RuntimeException::class);
        mockRequest('/public', null, ['NonExistingProvider' => []], [], $request, $config, $session, $socialLogin);
    }

    /**
     * Can we locate a non-existing provider
     *
     * @return void
     */
    public function testNotAProvider(): void
    {
        $this->expectException(\RuntimeException::class);
        mockRequest('/public', null, ['Notaprovider' => []], [], $request, $config, $session, $socialLogin);
    }

    /**
     * Tests routing
     *
     * @param string     $url            The URL to test.
     * @param array|null $expectedResult The expected result as [classFQCN, method].
     *
     * @dataProvider routerDataProvider
     * @return       void
     */
    public function testRouting(string $url, ?array $expectedResult): void
    {
        mockRequest($url, null, ['Email' => [], 'Dummy' => []], [], $request, $config, $session, $socialLogin);

        $this->assertCount(1, $socialLogin->getRouters());
        $router = $socialLogin->getRouters()[0];
        $routerResult = $router();

        if ($expectedResult === null) {
            $this->assertNull($routerResult);
        } else {
            $this->assertIsCallable($routerResult);
            $this->assertInstanceOf($expectedResult[0], $routerResult[0]);
            $this->assertEquals($expectedResult[1], $routerResult[1]);
        }
    }

    /**
     * Tests the error for autologin enabled with more then one provider
     *
     * @return void
     */
    public function testIllegalAutologin(): void
    {
        $request = new Request(['REQUEST_URI' => '/']);
        $config = Config::__set_state(
            ['configFolder' => __DIR__,
                'projectFolder' => __DIR__,
                'data' => [
                    'sociallogin' => [
                        'providers' => ['Email', 'Facebook'],
                        'autologin' => true,
                        'state_secret' => 'foobar'
                    ]
                ]
            ]
        );

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessageMatches('/^Autologin can\'t be enabled when there is more then 1 provider/');
        new SocialLogin($request, $config);
    }

    /**
     * Tests routing with invalid provider
     *
     * @return void
     */
    public function testRoutingInvalidProvider(): void
    {
        $url = '/_SOCIALLOGIN/login/404';
        mockRequest($url, null, ['Email' => []], [], $request, $config, $session, $socialLogin);

        $this->assertCount(1, $socialLogin->getRouters());
        $router = $socialLogin->getRouters()[0];
        $this->expectException(\RuntimeException::class);
        $router();
    }

    /**
     * Tests parseState() with no state secret
     *
     * @return void
     */
    public function testParseStateWithNoStateSecret(): void
    {
        // Set the state secret
        $reflection = new \ReflectionProperty(SocialLogin::class, 'stateSecret');
        $reflection->setAccessible(true);
        $reflection->setValue(null, null);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('No state secret configured. Please configure a state secret.');
        SocialLogin::parseState('');
    }

    /**
     * Tests generateState() with no state secret
     *
     * @return void
     */
    public function testGenerateStateWithNoStateSecret(): void
    {
        // Set the state secret
        $reflection = new \ReflectionProperty(SocialLogin::class, 'stateSecret');
        $reflection->setAccessible(true);
        $reflection->setValue(null, null);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('No state secret configured. Please configure a state secret.');
        SocialLogin::generateState([]);
    }

    /**
     * Test basic generate and parse state
     *
     * @return void
     */
    public function testGenerateParseState(): void
    {
        $state = SocialLogin::generateState(['foo' => 'bar']);
        $this->assertEquals(
            'eyJmb28iOiJiYXIiLCJ2YWxpZGF0ZSI6ImFhZmI1OWUxZDk5NTZkZDFjYTljMTU2YzRhZGRlOWU5OTVhMTBmN2EifQ==',
            $state
        );
        $newState = SocialLogin::parseState($state);
        $this->assertEquals('bar', $newState['foo']);
    }

    /**
     * Parse empty state
     *
     * @return void
     */
    public function testParseEmptyState(): void
    {
        $this->assertEquals([], SocialLogin::parseState(null));
    }

    /**
     * Empty states don't need validation
     *
     * @return void
     */
    public function testParseEmptyArrayState(): void
    {
        $this->assertEquals([], SocialLogin::parseState(base64_encode(json_encode([]))));
    }

    /**
     * Tampered state
     *
     * @return void
     */
    public function testParseTamperedState(): void
    {
        $this->expectException(\UnexpectedValueException::class);
        $this->expectExceptionMessage('The state looks tampered');
        SocialLogin::parseState(base64_encode(json_encode([
            'foo' => 'bar'
        ])));
    }
}
