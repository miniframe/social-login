<?php

namespace Miniframe\SocialLogin\Controller;

use Miniframe\Core\Registry;
use Miniframe\Middleware\Session;
use Miniframe\Response\PhpResponse;
use Miniframe\Response\RedirectResponse;
use Miniframe\SocialLogin\Middleware\SocialLogin;
use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
    /**
     * This method is called before each test.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // Set the state secret
        $reflection = new \ReflectionProperty(SocialLogin::class, 'stateSecret');
        $reflection->setAccessible(true);
        $reflection->setValue(null, 'foobar');
    }

    /**
     * Clears session at teardown
     *
     * @return void
     */
    protected function tearDown(): void
    {
        if (Registry::has(Session::class)) {
            Registry::get(Session::class)->commit();
        }
        Registry::register(Session::class, null);
    }

    /**
     * The Email page should throw an exception when no Email provider is configured
     *
     * @return void
     */
    public function testConstructorWithNoEmailProvider(): void
    {
        $state = SocialLogin::generateState(['redirectUrl' => '/socialLogin']);
        mockRequest(
            '/_SOCIALLOGIN/email?state=' . rawurlencode($state),
            null,
            ['Dummy' => []],
            [],
            $request,
            $config,
            $session,
            $socialLogin
        );

        $this->expectException(\RuntimeException::class);
        new Email($request, $config, $socialLogin, '/_SOCIALLOGIN/', $session);
    }

    /**
     * The sendCode action should throw an exception when no email is specified
     *
     * @return void
     */
    public function testSendCodeNoMail(): void
    {
        $state = SocialLogin::generateState(['redirectUrl' => '/socialLogin']);
        mockRequest(
            '/_SOCIALLOGIN/email/sendCode?state=' . rawurlencode($state),
            null,
            ['Email' => []],
            [],
            $request,
            $config,
            $session,
            $socialLogin
        );

        $controller = new Email($request, $config, $socialLogin, '/_SOCIALLOGIN/', $session);
        $this->expectException(RedirectResponse::class);
        $controller->sendCode(); // Redirect to login page to enter email address
    }

    /**
     * The sendCode action should show an error when no email is specified
     *
     * @return void
     */
    public function testSendCodeInvalidMail(): void
    {
        $state = SocialLogin::generateState(['redirectUrl' => '/socialLogin']);
        mockRequest(
            '/_SOCIALLOGIN/email/sendCode?state=' . rawurlencode($state),
            ['email' => 'invalid'],
            ['Email' => []],
            [],
            $request,
            $config,
            $session,
            $socialLogin
        );

        $controller = new Email($request, $config, $socialLogin, '/_SOCIALLOGIN/', $session);
        $this->assertInstanceOf(RedirectResponse::class, $controller->sendCode());
    }

    /**
     * Tests if the /code/ URL returns the correct response
     *
     * @return void
     */
    public function testCode(): void
    {
        $state = SocialLogin::generateState(['redirectUrl' => '/socialLogin']);
        mockRequest(
            '/_SOCIALLOGIN/email/code?state=' . rawurlencode($state),
            null,
            ['Email' => []],
            ['_SOCIALLOGIN' => ['email' => ['address' => 'foo@bar.baz']]],
            $request,
            $config,
            $session,
            $socialLogin
        );

        $controller = new Email($request, $config, $socialLogin, '/_SOCIALLOGIN/', $session);
        $this->assertInstanceOf(PhpResponse::class, $controller->code());
    }

    /**
     * Tests the signIn action with no code
     *
     * @return void
     */
    public function testSignInWithNoCode(): void
    {
        $state = SocialLogin::generateState(['redirectUrl' => '/socialLogin']);
        mockRequest(
            '/_SOCIALLOGIN/email/code?state=' . rawurlencode($state),
            null,
            ['Email' => []],
            [],
            $request,
            $config,
            $session,
            $socialLogin
        );

        $controller = new Email($request, $config, $socialLogin, '/_SOCIALLOGIN/', $session);
        $this->expectException(RedirectResponse::class);
        $controller->signIn();
    }

    /**
     * Tests the signIn action with a valid code
     *
     * @return void
     */
    public function testSignInWithValidCode(): void
    {
        $state = SocialLogin::generateState(['redirectUrl' => '/socialLogin']);
        mockRequest(
            '/_SOCIALLOGIN/email/code?state=' . rawurlencode($state),
            ['code' => 'ABC234'],
            ['Email' => []],
            [],
            $request,
            $config,
            $session,
            $socialLogin
        );
        $session->set('_SOCIALLOGIN', ['email' => [
            'address' => 'foo@bar.baz',
            'expires' => time() + 600,
            'code' => 'ABC234',
        ]]);

        $controller = new Email($request, $config, $socialLogin, '/_SOCIALLOGIN/', $session);
        $this->assertInstanceOf(RedirectResponse::class, $controller->signIn());
    }

    /**
     * Tests the signIn action with a valid code
     *
     * @return void
     */
    public function testSignInWithInvalidCode(): void
    {
        $state = SocialLogin::generateState(['redirectUrl' => '/socialLogin']);
        mockRequest(
            '/_SOCIALLOGIN/email/code?state=' . rawurlencode($state),
            ['code' => 'DEF567'],
            ['Email' => []],
            [],
            $request,
            $config,
            $session,
            $socialLogin
        );
        $session->set('_SOCIALLOGIN', ['email' => [
            'address' => 'foo@bar.baz',
            'expires' => time() + 600,
            'code' => 'ABC234',
        ]]);

        $controller = new Email($request, $config, $socialLogin, '/_SOCIALLOGIN/', $session);
        $this->assertInstanceOf(RedirectResponse::class, $controller->signIn());
    }
}
