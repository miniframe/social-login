<?php

namespace Miniframe\SocialLogin\Controller;

use Miniframe\Core\Registry;
use Miniframe\Middleware\Session;
use Miniframe\Response\PhpResponse;
use Miniframe\Response\RedirectResponse;
use Miniframe\SocialLogin\Middleware\SocialLogin;
use Miniframe\SocialLogin\Model\User;
use Miniframe\SocialLogin\Provider\Dummy;
use Miniframe\SocialLogin\Provider\Email;
use Miniframe\SocialLogin\Provider\Facebook;
use PHPUnit\Framework\TestCase;

class LoginTest extends TestCase
{
    /**
     * This method is called before each test.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // Set the state secret
        $reflection = new \ReflectionProperty(SocialLogin::class, 'stateSecret');
        $reflection->setAccessible(true);
        $reflection->setValue(null, 'foobar');
    }

    /**
     * Clears session at teardown
     *
     * @return void
     */
    protected function tearDown(): void
    {
        if (Registry::has(Session::class)) {
            Registry::get(Session::class)->commit();
        }
        Registry::register(Session::class, null);
    }

    /**
     * The login page should return a PhpResponse when a state is defined
     *
     * @return void
     */
    public function testLoginWithState(): void
    {
        $state = SocialLogin::generateState(['redirectUrl' => '/socialLogin']);
        mockRequest(
            '/_SOCIALLOGIN/login?state=' . rawurlencode($state),
            null,
            ['Email' => []],
            [],
            $request,
            $config,
            $session,
            $socialLogin
        );

        $controller = new Login($request, $config, $socialLogin, '/_SOCIALLOGIN/', array('email' => Email::class));
        $this->assertInstanceOf(PhpResponse::class, $controller->login());
    }

    /**
     * The login page should return a RedirectResponse when only one provider is enabled and autologin is true
     *
     * @return void
     */
    public function testAutologin(): void
    {
        $state = SocialLogin::generateState(['redirectUrl' => '/socialLogin']);
        mockRequest(
            '/_SOCIALLOGIN/login?state=' . rawurlencode($state),
            null,
            ['Facebook' => []],
            [],
            $request,
            $config,
            $session,
            $socialLogin,
            true
        );

        $controller = new Login(
            $request,
            $config,
            $socialLogin,
            '/_SOCIALLOGIN/',
            array('facebook' => Facebook::class)
        );
        $this->assertInstanceOf(RedirectResponse::class, $controller->login());
    }

    /**
     * The login page should return a RedirectResponse after executing a provider.
     *
     * @return void
     */
    public function testLoginWithProvider(): void
    {
        $state = SocialLogin::generateState(['redirectUrl' => '/socialLogin']);
        mockRequest(
            '/_SOCIALLOGIN/login?state=' . rawurlencode($state),
            null,
            ['Dummy' => []],
            [],
            $request,
            $config,
            $session,
            $socialLogin
        );

        $provider = new Dummy($request, $config);
        $controller = new Login(
            $request,
            $config,
            $socialLogin,
            '/_SOCIALLOGIN/',
            array('dummy' => Dummy::class),
            $provider
        );
        $this->assertInstanceOf(RedirectResponse::class, $controller->login());
    }

    /**
     * The login page should return a PhpResponse when no state is defined
     *
     * @return void
     */
    public function testLoginWithout(): void
    {
        mockRequest(
            '/_SOCIALLOGIN/login',
            null,
            ['Email' => []],
            [],
            $request,
            $config,
            $session,
            $socialLogin
        );

        $controller = new Login($request, $config, $socialLogin, '/_SOCIALLOGIN/', array('email' => Email::class));
        $this->assertInstanceOf(PhpResponse::class, $controller->login());
    }

    /**
     * Tests the logout page
     *
     * @return void
     */
    public function testLogout(): void
    {
        $state = SocialLogin::generateState(['redirectUrl' => '/socialLogin']);
        mockRequest(
            '/_SOCIALLOGIN/logout?state=' . rawurlencode($state),
            null,
            ['Email' => []],
            [],
            $request,
            $config,
            $session,
            $socialLogin
        );

        // Insert dummy user
        $user = new User('foo', 'bar', 'baz', 'foo', 'bar', ['baz']);
        $socialLogin->login($user);
        // Check if we are indeed logged in
        $this->assertInstanceOf(User::class, $socialLogin->getUser());

        // Log out, should result in redirect page
        $controller = new Login($request, $config, $socialLogin, '/_SOCIALLOGIN/', array('email' => Email::class));
        $this->assertInstanceOf(RedirectResponse::class, $controller->logout());
        $this->assertNull($socialLogin->getUser());
    }
}
