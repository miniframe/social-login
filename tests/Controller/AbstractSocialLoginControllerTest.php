<?php

namespace Miniframe\SocialLogin\Controller;

use Garrcomm\PHPUnitHelpers\FunctionMock;
use Miniframe\Core\Config;
use Miniframe\Core\Registry;
use Miniframe\Core\Request;
use Miniframe\Middleware\Session;
use Miniframe\SocialLogin\Middleware\SocialLogin;
use PHPUnit\Framework\TestCase;

class AbstractSocialLoginControllerTest extends TestCase
{
    /**
     * This method is called before each test.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        FunctionMock::mock('Miniframe\\Core', 'php_sapi_name', function () {
            return 'cgi';
        });
    }

    /**
     * This method is called after each test.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        FunctionMock::releaseAll();
        parent::tearDown();
        if (Registry::has(Session::class)) {
            Registry::get(Session::class)->commit();
            Registry::register(Session::class, null);
        }
    }

    /**
     * Returns all options for testDarkmode
     *
     * @return string[][]
     */
    public function darkmodeDataProvider(): array
    {
        return array(
           [true,   'on'],
           [false,  'off'],
           ['on',   'on'],
           ['off',  'off'],
           ['auto', 'auto'],
           ['ON',   'on'],
           ['OFF',  'off'],
           ['AUTO', 'auto'],
           ['foo',  null],
        );
    }

    /**
     * Tests dark mode configurations
     *
     * @param boolean|string $iniValue      The configuration value.
     * @param null|string    $expectedValue The expected value.
     *
     * @dataProvider darkmodeDataProvider
     *
     * @return void
     */
    public function testDarkmode(/* bool|string */ $iniValue, ?string $expectedValue): void
    {
        // Create prerequisites
        $providers = array();
        $request = new Request(['REQUEST_URI' => '/_SOCIALLOGIN/login']);
        $config = Config::__set_state([
            'configFolder' => __DIR__,
            'projectFolder' => __DIR__,
            'data' => [
                'framework' => [
                    'base_href' => '/',
                ],
                'sociallogin' => [
                    'state_secret' => 'foobar',
                    'providers' => $providers,
                    'darkmode' => $iniValue,
                ],
            ],
        ]);
        Registry::register(Session::class, new Session($request, $config));
        $middleware = new SocialLogin($request, $config);

        // When no value is expected, an exception is expected
        if ($expectedValue === null) {
            $this->expectException(\RuntimeException::class);
            $this->expectExceptionMessage('Invalid Dark Mode flag: ' . $iniValue);
        }

        // Create login controller
        $controller = new Login($request, $config, $middleware, '/_SOCIALLOGIN/', $providers);

        // Read and assert property
        $reflection = new \ReflectionClass($controller);
        $property = $reflection->getProperty('darkmode');
        $property->setAccessible(true);
        $this->assertEquals($expectedValue, $property->getValue($controller));
    }
}
