<?php

/**
 * PHPUnit bootstrapper
 */

use Miniframe\Core\Config;
use Miniframe\Core\Registry;
use Miniframe\Core\Request;
use Garrcomm\PHPUnitHelpers\FunctionMock;
use Miniframe\Middleware\Session;
use Miniframe\SocialLogin\Middleware\SocialLogin;

// Prevent error "Cannot start session when headers already sent"
ini_set("session.use_cookies", 0);
ini_set("session.use_only_cookies", 0);
ini_set("session.cache_limiter", "");

// Generic autoloader
if (!isset($GLOBALS['skipAutoload']) || $GLOBALS['skipAutoload'] !== true) {
    require_once __DIR__ . '/../vendor/autoload.php';
}

// Add abstract provider class
require_once __DIR__ . '/Provider/AbstractAbstractOAuth2ProviderTest.php';

// Add curl_exec() Emulator
require_once __DIR__ . '/curlEmulator.php';

// Add dummy classes
require_once __DIR__ . '/dummyClasses.php';

/**
 * Mocks a full request and config, required for several tests
 *
 * @param string           $requestUri  The requested URL.
 * @param array|null       $postValues  Post values for the request.
 * @param array            $providers   Enabled providers.
 * @param array            $sessionData Initial session state.
 * @param Request|null     $request     Returns a Request object.
 * @param Config|null      $config      Returns a Config object.
 * @param Session|null     $session     Returns a Session object.
 * @param SocialLogin|null $socialLogin Returns a SocialLogin middleware.
 * @param boolean          $autologin   Set to true to enable Autologin.
 *
 * @return void
 */
function mockRequest(
    string $requestUri,
    ?array $postValues,
    array $providers,
    array $sessionData,
    ?Request &$request,
    ?Config &$config,
    ?Session &$session,
    ?SocialLogin &$socialLogin,
    bool $autologin = false
): void {
    // Used to emulate php_sapi_name()
    FunctionMock::mock('Miniframe\\Core', 'php_sapi_name', function () {
        return 'cgi';
    });

    // Fetches the query path and converts it to a $_GET array
    parse_str(parse_url($requestUri, PHP_URL_QUERY), $requestValues);

    // Create dummy request
    $request = new Request(
        ['REQUEST_URI' => $requestUri],
        $requestValues,
        $postValues ?? []
    );

    // Convert provider configs
    $providerConfigs = array();
    foreach ($providers as $provider => $config) {
        $providerConfigs['sociallogin-' . strtolower($provider)] = $config;
    }

    // Create dummy config
    $config = Config::__set_state([
            'configFolder' => __DIR__,
            'projectFolder' => __DIR__,
            'data' => array_merge([
            'framework' => ['base_href' => '/'],
            'sociallogin' => [
                'state_secret' => 'foobar',
                'providers' => array_keys($providers),
                'public_href' => '/public/',
                'exclude' => ['/public', '/public/*'],
                'autologin' => $autologin,
            ],
            ], $providerConfigs)
    ]);

    // Create dummy session
    $session = new Session($request, $config);
    Registry::register(Session::class, $session);
    foreach ($sessionData as $key => $value) {
        $session->set($key, $value);
    }

    // Create dummy SocialLogin middleware
    $socialLogin = new SocialLogin($request, $config);
}
