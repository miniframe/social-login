<?php

namespace Miniframe\SocialLogin\Provider;

class LinkedinTest extends AbstractAbstractOAuth2ProviderTest
{
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName = 'linkedin';

    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN = Linkedin::class;

    /**
     * Tests getLogoSource
     *
     * @return void
     */
    public function testGetLogoSource(): void
    {
        $this->assertStringStartsWith('data:image/svg+xml;base64,', Linkedin::getLogoSource());
    }

    /**
     * Tests getThemeColor
     *
     * @return void
     */
    public function testGetThemeColor(): void
    {
        $this->assertEquals('rgb(40,103,178)', Linkedin::getThemeColor());
    }

    /**
     * Simulates a cURL call
     *
     * @param resource $ch       CURL resource handler.
     * @param array    $curlInfo CURL info.
     *
     * @return mixed
     */
    public function curlResponse($ch, array &$curlInfo)
    {
        $curlInfo['http_code'] = 200;
        if ($curlInfo['url'] == 'https://www.linkedin.com/oauth/v2/accessToken') {
            return json_encode(['access_token' => 'foobar']);
        }
        if (
            $curlInfo['url'] == 'https://api.linkedin.com/v2/me?projection=(id,localizedFirstName,localizedLastName,'
            . 'profilePicture(displayImage~:playableStreams))'
        ) {
            $profileData = [
                'id' => 'foo',
                'localizedFirstName' => 'Foo',
                'localizedLastName' => 'Bar',
            ];
            if ($this->hasProfilePicture) {
                $profileData['profilePicture'] = [
                    'displayImage~' => [
                        'elements' => [
                            [
                                'identifiers' => [
                                    [
                                        'identifier' => 'https://foo.bar.baz/',
                                        'identifierType' => 'EXTERNAL_URL',
                                    ]
                                ],
                            ]
                        ]
                    ]
                ];
            }

            return json_encode($profileData);
        }
    }

    /**
     * The username is the same as the unique key for LinkedIn
     *
     * @param string $uniqueKey   The expected unique key.
     * @param string $username    The expected username.
     * @param string $displayName The expected display name.
     * @param string $avatar      The expected avatar.
     *
     * @return void
     */
    public function testAuthenticateStep2(
        string $uniqueKey = 'foo',
        string $username = 'foo@bar.baz',
        string $displayName = 'Foo Bar',
        string $avatar = 'https://foo.bar.baz/'
    ): void {
        $username = $uniqueKey;
        $this->hasProfilePicture = true;
        parent::testAuthenticateStep2($uniqueKey, $username, $displayName, $avatar);
    }

    /**
     * Set to false to disable the profile picture
     *
     * @var bool
     */
    protected $hasProfilePicture = true;

    /**
     * Tests with no profile picture
     *
     * @return void
     */
    public function testAuthenticateStep2NoProfile(): void
    {
        $this->hasProfilePicture = false;
        $avatar = 'data:image/svg+xml;base64,'
            . base64_encode(file_get_contents(__DIR__ . '/../../templates/unknown-user.svg'));
        parent::testAuthenticateStep2('foo', 'foo', 'Foo Bar', $avatar);
    }
}
