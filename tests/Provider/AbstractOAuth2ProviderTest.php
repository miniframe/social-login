<?php

namespace Miniframe\SocialLogin\Provider;

use Garrcomm\PHPUnitHelpers\FunctionMock;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Response\RedirectResponse;
use Miniframe\SocialLogin\Middleware\SocialLogin;
use Miniframe\SocialLogin\Model\User;
use PHPUnit\Framework\TestCase;

/**
 * Most of the abstract class is tested by testing the providers themselves,
 * but some cases are not covered by those tests (mostly the error scenarios).
 *
 * This test uses Bitbucket as provider, but that could be any other OAuth2 provider.
 * Also, the Dummy provider is used in some specific cases.
 */
class AbstractOAuth2ProviderTest extends TestCase
{
    /**
     * Dummy config.
     *
     * @var Config
     */
    protected $config;
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName = 'bitbucket';
    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN = Bitbucket::class;

    /**
     * This method is called before each test.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        // Defines a generic config
        $this->config = Config::__set_state([
            'configFolder' => __DIR__,
            'projectFolder' => __DIR__,
            'data' => [
                'framework' => ['base_href' => '/'],
                'sociallogin' => ['state_secret' => 'foobar'],
                'sociallogin-' . $this->providerName => [
                    'client_id' => 'foo',
                    'client_secret' => 'bar'
                ],
                'sociallogin-dummyoauth2provider' => [
                    'client_id' => 'foo',
                    'client_secret' => 'bar'
                ],
            ],
        ]);
        // Requests should be webbased/cgi
        FunctionMock::mock('Miniframe\\Core', 'php_sapi_name', function () {
            return 'cgi';
        });

        // Set the state secret
        $reflection = new \ReflectionProperty(SocialLogin::class, 'stateSecret');
        $reflection->setAccessible(true);
        $reflection->setValue(null, 'foobar');
    }

    /**
     * This method is called after each test.
     *
     * @return void
     */
    public function tearDown(): void
    {
        FunctionMock::releaseAll();
        parent::tearDown();
    }

    /**
     * Tests the Invalid State error
     *
     * @return void
     */
    public function testInvalidState(): void
    {
        $state = SocialLogin::generateState(['foo' => 'bar']);
        $request = new Request(
            ['REQUEST_URI' => '/_SOCIALLOGIN/login?state=' . $state . '&code=foo'],
            ['state' => $state, 'code' => 'foo']
        );

        $provider = new $this->providerFQCN($request, $this->config); /* @var $provider AbstractOAuth2Provider */
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('State incomplete');
        $provider->authenticate();
    }

    /**
     * Tests the No Access Token error
     *
     * @return void
     */
    public function testNoAccesstoken(): void
    {
        setCurlExecMock(function ($ch, array &$curlInfo) {
            $curlInfo['http_code'] = 200;
            return json_encode(['not_an_access_token' => 'foobar']);
        });

        $state = SocialLogin::generateState(['redirectUrl' => 'https://foo.bar.baz/']);
        $request = new Request(
            ['REQUEST_URI' => '/_SOCIALLOGIN/login?state=' . $state . '&code=foo'],
            ['state' => $state, 'code' => 'foo']
        );

        $provider = new $this->providerFQCN($request, $this->config); /* @var $provider AbstractOAuth2Provider */
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Did\'t get an access token');
        $provider->authenticate();
    }

    /**
     * Most OAuth providers have URLs that don't contain a ? but we should still support it.
     *
     * @return void
     */
    public function testQueryBuilder(): void
    {
        setCurlExecMock(function ($ch, array &$curlInfo) {
            $curlInfo['http_code'] = 200;
            return json_encode(['access_token' => 'foobar']);
        });

        $state = SocialLogin::generateState(['redirectUrl' => 'https://foo.bar.baz/']);
        $request = new Request(
            ['REQUEST_URI' => '/_SOCIALLOGIN/login?state=' . $state . '&code=foo', 'HTTPS' => 'on'],
            ['state' => $state, 'code' => 'foo']
        );

        DummyOAuth2Provider::$testcase = 'curlGetWithDataArray';
        $provider = new DummyOAuth2Provider($request, $this->config);
        $this->assertInstanceOf(User::class, $provider->authenticate());
    }

    /**
     * When an invalid request method is specified we want an error
     *
     * @return void
     */
    public function testInvalidRequestMethod(): void
    {
        $state = SocialLogin::generateState(['redirectUrl' => 'https://foo.bar.baz/']);
        $request = new Request(
            ['REQUEST_URI' => '/_SOCIALLOGIN/login?state=' . $state . '&code=foo'],
            ['state' => $state, 'code' => 'foo']
        );

        DummyOAuth2Provider::$testcase = 'curlWithInvalidRequestMethod';
        $provider = new DummyOAuth2Provider($request, $this->config);
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Can\'t handle request method "TEAPOT"');
        $provider->authenticate();
    }

    /**
     * When a request failed, we want an error
     *
     * @return void
     */
    public function testFailedRequest(): void
    {
        setCurlExecMock(function ($ch, array &$curlInfo) {
            return false;
        });

        $state = SocialLogin::generateState(['redirectUrl' => 'https://foo.bar.baz/']);
        $request = new Request(
            ['REQUEST_URI' => '/_SOCIALLOGIN/login?state=' . $state . '&code=foo'],
            ['state' => $state, 'code' => 'foo']
        );

        $provider = new DummyOAuth2Provider($request, $this->config);
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Unexpected cURL error #0: ');
        $provider->authenticate();
    }

    /**
     * When a request failed, we want an error
     *
     * @return void
     */
    public function testInvalidResponse(): void
    {
        setCurlExecMock(function ($ch, array &$curlInfo) {
            $curlInfo['http_code'] = 200;
            $curlInfo['content_type'] = 'image/jpeg';
            return 'foobar';
        });

        $state = SocialLogin::generateState(['redirectUrl' => 'https://foo.bar.baz/']);
        $request = new Request(
            ['REQUEST_URI' => '/_SOCIALLOGIN/login?state=' . $state . '&code=foo'],
            ['state' => $state, 'code' => 'foo']
        );

        $provider = new DummyOAuth2Provider($request, $this->config);
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Could not decode the image/jpeg response');
        $provider->authenticate();
    }

    /**
     * When a request failed, we want an error
     *
     * @return void
     */
    public function testRedirectUrl(): void
    {
        setCurlExecMock(function ($ch, array &$curlInfo) {
            $curlInfo['http_code'] = 200;
            $curlInfo['content_type'] = 'image/jpeg';
            return 'foobar';
        });

        $state = SocialLogin::generateState(['redirectUrl' => 'https://foo.bar.baz/']);
        $request = new Request(
            ['REQUEST_URI' => '/_SOCIALLOGIN/login?state=' . $state],
            ['state' => $state]
        );

        $provider = new DummyOAuth2Provider($request, $this->config);
        $this->expectException(RedirectResponse::class);
        $provider->authenticate();
    }
}
