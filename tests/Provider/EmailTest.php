<?php

namespace Miniframe\SocialLogin\Provider;

use Garrcomm\PHPUnitHelpers\FunctionMock;
use Miniframe\Core\Config;
use Miniframe\Core\Registry;
use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Mailer\Middleware\Mailer;
use Miniframe\Mailer\Model\Attachment;
use Miniframe\Mailer\Model\Recipient;
use Miniframe\Middleware\Session;
use Miniframe\Response\RedirectResponse;
use Miniframe\SocialLogin\Model\User;
use PHPMailer\PHPMailer\Exception;
use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
    /**
     * Dummy config.
     *
     * @var Config
     */
    protected $config;

    /**
     * Reference to the Session object
     *
     * @var Session|null
     */
    protected $session;

    /**
     * This method is called before each test.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        // Defines a generic config
        $this->config = Config::__set_state([
            'configFolder' => __DIR__,
            'projectFolder' => __DIR__,
            'data' => [
                'framework' => ['base_href' => '/'],
                'sociallogin-email' => [
                    'from_name' => 'Miniframe Social Login',
                    'from_email' => 'miniframe@example.com',
                ],
            ],
        ]);
        // Requests should be webbased/cgi
        FunctionMock::mock('Miniframe\\Core', 'php_sapi_name', function () {
            return 'cgi';
        });
    }

    /**
     * This method is called after each test.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        FunctionMock::releaseAll();
        parent::tearDown();

        if ($this->session !== null) {
            $this->session->commit();
            $this->session = null;
        }

        Registry::register(Mailer::class, null);
    }

    /**
     * Tests getLogoSource
     *
     * @return void
     */
    public function testGetLogoSource(): void
    {
        $this->assertStringStartsWith('data:image/svg+xml;base64,', Email::getLogoSource());
    }

    /**
     * Tests getThemeColor
     *
     * @return void
     */
    public function testGetThemeColor(): void
    {
        $this->assertEquals('#c0c0c0', Email::getThemeColor());
    }

    /**
     * This provider does follow the ProviderInterface, but also requires additional parameters.
     *
     * @return void
     */
    public function testInvalidInstance(): void
    {
        $this->expectException(\BadFunctionCallException::class);
        new Email(new Request(['REQUEST_URI' => '/']), $this->config);
    }

    /**
     * For the Email provider, the first step should redirect to the page to fill in a mail address.
     *
     * @return void
     */
    public function testAuthenticateStep1(): void
    {
        $request = new Request(['REQUEST_URI' => '/']);
        $this->session = new Session($request, $this->config);
        $provider = new Email($request, $this->config, $this->session, '/_SOCIALLOGIN/');

        $this->expectException(RedirectResponse::class);
        $provider->authenticate();
    }

    /**
     * For the Email provider, the second step should send an email and redirect to the page to fill in the code.
     *
     * @return void
     */
    public function testAuthenticateStep2(): void
    {
        $request = new Request(['REQUEST_URI' => '/'], [], ['email' => 'foo@bar.baz']);
        $this->session = new Session($request, $this->config);
        $provider = new Email($request, $this->config, $this->session, '/_SOCIALLOGIN/');

        FunctionMock::mock(__NAMESPACE__, 'mail', function () {
            return true;
        });

        $this->expectException(RedirectResponse::class);
        $provider->authenticate();
    }

    /**
     * Send the code through the Mailer bundle
     *
     * @return void
     */
    public function testAuthenticateStep2MailerBundle(): void
    {
        $request = new Request(['REQUEST_URI' => '/'], [], ['email' => 'foo@bar.baz']);
        $this->session = new Session($request, $this->config);
        $provider = new Email($request, $this->config, $this->session, '/_SOCIALLOGIN/');

        Registry::register(Mailer::class, new class ($request, $this->config) extends Mailer {
            /**
             * Dummy test method
             *
             * @param Recipient|Recipient[]        $recipient      One or more recipients.
             * @param string                       $subject        Subject of the mail.
             * @param Response                     $mailBody       A response object that returns a mail body.
             * @param boolean                      $mailBodyIsHtml Set to false if the mail is plain text.
             * @param Attachment|Attachment[]|null $attachment     None, one or more attachments.
             * @param array                        $options        Additional options.
             *
             * @return void
             */
            public function sendMail(
                $recipient,
                string $subject,
                Response $mailBody,
                bool $mailBodyIsHtml = true,
                $attachment = null,
                array $options = array()
            ): void {
            }
        });

        $this->expectException(RedirectResponse::class);
        $provider->authenticate();
    }

    /**
     * When specifying an invalid mail address, an exception should be thrown
     *
     * @return void
     */
    public function testAuthenticateStep2InvalidMail(): void
    {
        $request = new Request(['REQUEST_URI' => '/'], [], ['email' => 'foobar.baz']);
        $this->session = new Session($request, $this->config);
        $provider = new Email($request, $this->config, $this->session, '/_SOCIALLOGIN/');

        $this->expectException(\InvalidArgumentException::class);
        $provider->authenticate();
    }

    /**
     * When sending the mail fails, an exception should be thrown
     *
     * @return void
     */
    public function testAuthenticateStep2CantSendMail(): void
    {
        $request = new Request(['REQUEST_URI' => '/'], [], ['email' => 'foo@bar.baz']);
        $this->session = new Session($request, $this->config);
        $provider = new Email($request, $this->config, $this->session, '/_SOCIALLOGIN/');

        FunctionMock::mock(__NAMESPACE__, 'mail', function () {
            return false;
        });

        $this->expectException(\RuntimeException::class);
        $provider->authenticate();
    }

    /**
     * For the Email provider, the third step should verify the code and return a User object.
     *
     * @return void
     */
    public function testAuthenticateStep3(): void
    {
        $request = new Request(['REQUEST_URI' => '/'], [], ['code' => 'F00B4R']);
        $this->session = new Session($request, $this->config);
        $this->session->set('_SOCIALLOGIN', [
            'email' => [
                'address' => 'foo@bar.baz',
                'code' => 'F00B4R',
                'expires' => time() + 600,
            ]
        ]);
        $provider = new Email($request, $this->config, $this->session, '/_SOCIALLOGIN/');
        $user = $provider->authenticate();

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals('foo@bar.baz', $user->getUniqueKey());
        $this->assertEquals('foo@bar.baz', $user->getUserName());
        $this->assertEquals('Foo', $user->getDisplayName());
    }

    /**
     * When people press back, the session will be broken and an exception will be thrown.
     *
     * @return void
     */
    public function testAuthenticateStep3BrokenSession(): void
    {
        $request = new Request(['REQUEST_URI' => '/'], [], ['code' => 'F00B4R']);
        $this->session = new Session($request, $this->config);
        $provider = new Email($request, $this->config, $this->session, '/_SOCIALLOGIN/');

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('No session available. Please try again.');
        $provider->authenticate();
    }

    /**
     * When the session is expired, an exception should be thrown
     *
     * @return void
     */
    public function testAuthenticateStep3Expired(): void
    {
        $request = new Request(['REQUEST_URI' => '/'], [], ['code' => 'F00B4R']);
        $this->session = new Session($request, $this->config);
        $this->session->set('_SOCIALLOGIN', [
            'email' => [
                'address' => 'foo@bar.baz',
                'code' => 'F00B4R',
                'expires' => time() - 600,
            ]
        ]);
        $provider = new Email($request, $this->config, $this->session, '/_SOCIALLOGIN/');

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Session expired. Please try again.');
        $provider->authenticate();
    }
    /**
     * When the code is invalid, an exception should be thrown
     *
     * @return void
     */
    public function testAuthenticateStep3InvalidCode(): void
    {
        $request = new Request(['REQUEST_URI' => '/'], [], ['code' => 'FOOBAR']);
        $this->session = new Session($request, $this->config);
        $this->session->set('_SOCIALLOGIN', [
            'email' => [
                'address' => 'foo@bar.baz',
                'code' => 'F00B4R',
                'expires' => time() + 600,
            ]
        ]);
        $provider = new Email($request, $this->config, $this->session, '/_SOCIALLOGIN/');

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid code. Please try again.');
        $provider->authenticate();
    }
}
