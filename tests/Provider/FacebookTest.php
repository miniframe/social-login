<?php

namespace Miniframe\SocialLogin\Provider;

class FacebookTest extends AbstractAbstractOAuth2ProviderTest
{
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName = 'facebook';

    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN = Facebook::class;

    /**
     * Tests getLogoSource
     *
     * @return void
     */
    public function testGetLogoSource(): void
    {
        $this->assertStringStartsWith('data:image/svg+xml;base64,', Facebook::getLogoSource());
    }

    /**
     * Tests getThemeColor
     *
     * @return void
     */
    public function testGetThemeColor(): void
    {
        $this->assertEquals('rgba(49,135,255,255)', Facebook::getThemeColor());
    }

    /**
     * Simulates a cURL call
     *
     * @param resource $ch       CURL resource handler.
     * @param array    $curlInfo CURL info.
     *
     * @return mixed
     */
    public function curlResponse($ch, array &$curlInfo)
    {
        $curlInfo['http_code'] = 200;
        if ($curlInfo['url'] == 'https://graph.facebook.com/v11.0/oauth/access_token') {
            return json_encode(['access_token' => 'foobar']);
        }
        if ($curlInfo['url'] == 'https://graph.facebook.com/me') {
            return json_encode([
                'id' => 'foo',
                'name' => 'Foo Bar',
                'links' => ['avatar' => ['href' => 'https://foo.bar.baz/']],
            ]);
        }
        $imageUrl = 'https://graph.facebook.com/v11.0/me/picture?format=json&redirect=false&height=128&width=128';
        if ($curlInfo['url'] == $imageUrl) {
            return json_encode([
                'data' => ['url' => 'https://foo.bar.baz/image.jpg'],
            ]);
        }
        if ($curlInfo['url'] == 'https://foo.bar.baz/image.jpg') {
            $curlInfo['content_type'] = 'image/jpeg';
            return 'foobar';
        }
    }

    /**
     * Facebook overrules the username and the avatar
     *
     * @param string $uniqueKey   The expected unique key.
     * @param string $username    The expected username.
     * @param string $displayName The expected display name.
     * @param string $avatar      The expected avatar.
     *
     * @return void
     */
    public function testAuthenticateStep2(
        string $uniqueKey = 'foo',
        string $username = 'foo@bar.baz',
        string $displayName = 'Foo Bar',
        string $avatar = 'https://foo.bar.baz/'
    ): void {
        $username = $uniqueKey;
        $avatar = 'data:image/jpeg;base64,Zm9vYmFy';
        parent::testAuthenticateStep2($uniqueKey, $username, $displayName, $avatar);
    }
}
