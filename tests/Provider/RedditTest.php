<?php

namespace Miniframe\SocialLogin\Provider;

class RedditTest extends AbstractAbstractOAuth2ProviderTest
{
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName = 'reddit';

    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN = Reddit::class;

    /**
     * Tests getLogoSource
     *
     * @return void
     */
    public function testGetLogoSource(): void
    {
        $this->assertStringStartsWith('data:image/svg+xml;base64,', Reddit::getLogoSource());
    }

    /**
     * Tests getThemeColor
     *
     * @return void
     */
    public function testGetThemeColor(): void
    {
        $this->assertEquals('rgb(255,99,20)', Reddit::getThemeColor());
    }

    /**
     * Simulates a cURL call
     *
     * @param resource $ch       CURL resource handler.
     * @param array    $curlInfo CURL info.
     *
     * @return mixed
     */
    public function curlResponse($ch, array &$curlInfo)
    {
        $curlInfo['http_code'] = 200;
        if ($curlInfo['url'] == 'https://www.reddit.com/api/v1/access_token') {
            return json_encode(['access_token' => 'foobar']);
        }
        if ($curlInfo['url'] == 'https://oauth.reddit.com/api/v1/me') {
            return json_encode([
                'id' => 'foo',
                'name' => 'foo@bar.baz',
                'subreddit' => [
                    'user_is_banned' => $this->userIsBanned,
                    'title' => 'Foo Bar',
                ],
                'icon_img' => 'https://foo.bar.baz/',
                'has_verified_email' => $this->hasVerifiedEmail,
            ]);
        }
    }

    /**
     * Sets the result for cURL requests
     *
     * @var bool
     */
    protected $userIsBanned = false;
    /**
     * Sets the result for cURL requests
     *
     * @var bool
     */
    protected $hasVerifiedEmail = true;

    /**
     * Reddit test with active user
     *
     * @param string $uniqueKey   The expected unique key.
     * @param string $username    The expected username.
     * @param string $displayName The expected display name.
     * @param string $avatar      The expected avatar.
     *
     * @return void
     */
    public function testAuthenticateStep2(
        string $uniqueKey = 'foo',
        string $username = 'foo@bar.baz',
        string $displayName = 'Foo Bar',
        string $avatar = 'https://foo.bar.baz/'
    ): void {
        $this->userIsBanned = false;
        $this->hasVerifiedEmail = true;
        parent::testAuthenticateStep2($uniqueKey, $username, $displayName, $avatar);
    }

    /**
     * Reddit test with banned user
     *
     * @return void
     */
    public function testAuthenticateStep2Banned(): void
    {
        $this->userIsBanned = true;
        $this->hasVerifiedEmail = true;
        $this->expectException(\RuntimeException::class);
        parent::testAuthenticateStep2();
    }

    /**
     * Reddit test with unverified user
     *
     * @return void
     */
    public function testAuthenticateStep2NotVerified(): void
    {
        $this->userIsBanned = false;
        $this->hasVerifiedEmail = false;
        $this->expectException(\RuntimeException::class);
        parent::testAuthenticateStep2();
    }
}
