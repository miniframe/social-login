<?php

namespace Miniframe\SocialLogin\Provider;

use Garrcomm\PHPUnitHelpers\FunctionMock;
use Miniframe\SocialLogin\Middleware\SocialLogin;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Response\RedirectResponse;
use Miniframe\SocialLogin\Model\User;
use PHPUnit\Framework\TestCase;

abstract class AbstractAbstractOAuth2ProviderTest extends TestCase
{
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName;

    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN;

    /**
     * Simulates a cURL call
     *
     * @param resource $ch       CURL resource handler.
     * @param array    $curlInfo CURL info.
     *
     * @return mixed
     */
    abstract public function curlResponse($ch, array &$curlInfo);

    /**
     * Tests getLogoSource
     *
     * @return void
     */
    abstract public function testGetLogoSource(): void;

    /**
     * Tests getThemeColor
     *
     * @return void
     */
    abstract public function testGetThemeColor(): void;

    /**
     * This method is called before each test.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        if (!$this->providerName || !$this->providerFQCN) {
            throw new \RuntimeException('Provider name must be specified');
        }

        // Defines a generic config
        $this->config = Config::__set_state([
            'configFolder' => __DIR__,
            'projectFolder' => __DIR__,
            'data' => [
                'framework' => ['base_href' => '/'],
                'sociallogin' => ['state_secret' => 'foobar'],
                'sociallogin-' . $this->providerName => [
                    'client_id' => 'foo',
                    'client_secret' => 'bar'
                ]
            ],
        ]);
        // Requests should be webbased/cgi
        FunctionMock::mock('Miniframe\\Core', 'php_sapi_name', function () {
            return 'cgi';
        });

        // Set the state secret
        $reflection = new \ReflectionProperty(SocialLogin::class, 'stateSecret');
        $reflection->setAccessible(true);
        $reflection->setValue(null, 'foobar');
    }

    /**
     * This method is called after each test.
     *
     * @return void
     */
    public function tearDown(): void
    {
        FunctionMock::releaseAll();
        parent::tearDown();
    }

    /**
     * Dummy config.
     *
     * @var Config
     */
    protected $config;
    /**
     * For an OAuth 2.0 provider, the first step should redirect to an authorize URL
     *
     * @return void
     */
    public function testAuthenticateStep1(): void
    {
        $provider = new $this->providerFQCN(new Request(['REQUEST_URI' => '/']), $this->config);
        $this->expectException(RedirectResponse::class);
        $provider->authenticate();
    }

    /**
     * For an OAuth 2.0 provider, the second step should fetch data based on the access token
     *
     * @param string $uniqueKey   The expected unique key.
     * @param string $username    The expected username.
     * @param string $displayName The expected display name.
     * @param string $avatar      The expected avatar.
     *
     * @return void
     */
    public function testAuthenticateStep2(
        string $uniqueKey = 'foo',
        string $username = 'foo@bar.baz',
        string $displayName = 'Foo Bar',
        string $avatar = 'https://foo.bar.baz/'
    ): void {
        $state = SocialLogin::generateState(['redirectUrl' => '/']);
        $code = 'foobar';

        setCurlExecMock([$this, 'curlResponse']);

        $request = new Request(['REQUEST_URI' => '/?state=' . rawurlencode($state) . '&code=' . rawurlencode($code)], [
            'state' => $state,
            'code' => $code
        ]);
        $provider = new $this->providerFQCN($request, $this->config);
        $user = $provider->authenticate();

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($uniqueKey, $user->getUniqueKey());
        $this->assertEquals($username, $user->getUserName());
        $this->assertEquals($displayName, $user->getDisplayName());
        $this->assertEquals($avatar, $user->getAvatar());
        $this->assertEquals($this->providerFQCN, $user->getProviderFQCN());
        $this->assertIsArray($user->getRawData());
    }

    /**
     * Tests getCurrentUri with a port number in the hostname
     *
     * @return void
     */
    public function testGetCurrentUriWithPortInHost(): void
    {
        // Registers middlewares
        $request = new Request(
            [
                'REQUEST_URI' => '/_SOCIALLOGIN/login/' . $this->providerName,
                'HTTP_HOST' => 'localhost:81',
            ],
        );
        $provider = new $this->providerFQCN($request, $this->config); /* @var $provider AbstractOAuth2Provider */
        $this->expectException(RedirectResponse::class);
        $provider->authenticate();
    }
}
