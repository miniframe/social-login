<?php

namespace Miniframe\SocialLogin\Provider;

class MicrosoftTest extends AbstractAbstractOAuth2ProviderTest
{
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName = 'microsoft';

    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN = Microsoft::class;

    /**
     * Tests getLogoSource
     *
     * @return void
     */
    public function testGetLogoSource(): void
    {
        $this->assertStringStartsWith('data:image/svg+xml;base64,', Microsoft::getLogoSource());
    }

    /**
     * Tests getThemeColor
     *
     * @return void
     */
    public function testGetThemeColor(): void
    {
        $this->assertEquals('rgb(0, 103, 184)', Microsoft::getThemeColor());
    }

    /**
     * Simulates a cURL call
     *
     * @param resource $ch       CURL resource handler.
     * @param array    $curlInfo CURL info.
     *
     * @return mixed
     */
    public function curlResponse($ch, array &$curlInfo)
    {
        $curlInfo['http_code'] = 200;
        if ($curlInfo['url'] == 'https://login.microsoftonline.com/common/oauth2/v2.0/token') {
            return json_encode(['access_token' => 'foobar']);
        }
        if ($curlInfo['url'] == 'https://graph.microsoft.com/v1.0/me') {
            return json_encode([
                'id' => 'foo',
                'userPrincipalName' => 'foo@bar.baz',
                'displayName' => 'Foo Bar',
            ]);
        }
        if ($curlInfo['url'] == 'https://graph.microsoft.com/v1.0/me/photo/$value' && $this->serverError) {
            $curlInfo['http_code'] = 500;
            return 'internal server error';
        }
        if ($curlInfo['url'] == 'https://graph.microsoft.com/v1.0/me/photo/$value' && $this->imageStep == 1) {
            $curlInfo['content_type'] = 'image/jpeg';
            return 'foobar';
        }
        if ($curlInfo['url'] == 'https://graph.microsoft.com/v1.0/me/photo/$value' && $this->imageStep > 1) {
            $curlInfo['http_code'] = 404;
            return 'not found';
        }
        if ($curlInfo['url'] == 'https://graph.microsoft.com/beta/me/photo/$value' && $this->imageStep == 2) {
            $curlInfo['content_type'] = 'image/jpeg';
            return 'foobar';
        }
    }

    /**
     * Image step, test all possibilities
     *
     * @var int
     */
    protected $imageStep = 1;
    /**
     * Simulates a server error when requesting an image
     *
     * @var bool
     */
    protected $serverError = false;

    /**
     * For Microsoft we embed the avatar
     *
     * @param string $uniqueKey   The expected unique key.
     * @param string $username    The expected username.
     * @param string $displayName The expected display name.
     * @param string $avatar      The expected avatar.
     *
     * @return void
     */
    public function testAuthenticateStep2(
        string $uniqueKey = 'foo',
        string $username = 'foo@bar.baz',
        string $displayName = 'Foo Bar',
        string $avatar = 'https://foo.bar.baz/'
    ): void {
        $avatar = 'data:image/jpeg;base64,Zm9vYmFy';
        $this->imageStep = 1;
        $this->serverError = false;
        parent::testAuthenticateStep2($uniqueKey, $username, $displayName, $avatar);
    }

    /**
     * Private accounts have images in another URL
     *
     * @return void
     */
    public function testAuthenticateStep2WithPrivateAccount(): void
    {
        $this->imageStep = 2;
        $this->serverError = false;
        parent::testAuthenticateStep2('foo', 'foo@bar.baz', 'Foo Bar', 'data:image/jpeg;base64,Zm9vYmFy');
    }

    /**
     * When something strange happens, an exception is thrown
     *
     * @return void
     */
    public function testAuthenticateStep2WithInternalServerError(): void
    {
        $this->imageStep = 1;
        $this->serverError = true;
        $this->expectException(\RuntimeException::class);
        parent::testAuthenticateStep2();
    }
}
