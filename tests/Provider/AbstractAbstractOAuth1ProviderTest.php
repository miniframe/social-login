<?php

namespace Miniframe\SocialLogin\Provider;

use Garrcomm\PHPUnitHelpers\FunctionMock;
use Miniframe\Core\Config;
use Miniframe\Core\Registry;
use Miniframe\Core\Request;
use Miniframe\Middleware\Session;
use Miniframe\Response\RedirectResponse;
use Miniframe\SocialLogin\Middleware\SocialLogin;
use PHPUnit\Framework\TestCase;

abstract class AbstractAbstractOAuth1ProviderTest extends TestCase
{
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName;

    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN;

    /**
     * Tests getLogoSource
     *
     * @return void
     */
    abstract public function testGetLogoSource(): void;

    /**
     * Tests getThemeColor
     *
     * @return void
     */
    abstract public function testGetThemeColor(): void;

    /**
     * Simulates a cURL call
     *
     * @param resource $ch       CURL resource handler.
     * @param array    $curlInfo CURL info.
     *
     * @return mixed
     */
    abstract public function curlResponse($ch, array &$curlInfo);

    /**
     * Reference to the Config object
     *
     * @var Config
     */
    protected $config;

    /**
     * This method is called before each test.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        if (!$this->providerName || !$this->providerFQCN) {
            throw new \RuntimeException('Provider name must be specified');
        }

        // Defines a generic config
        $this->config = Config::__set_state([
            'configFolder' => __DIR__,
            'projectFolder' => __DIR__,
            'data' => [
                'framework' => ['base_href' => '/'],
                'sociallogin' => ['state_secret' => 'foobar', 'providers' => [$this->providerName]],
                'sociallogin-' . $this->providerName => [
                    'consumer_key' => 'foo',
                    'consumer_secret' => 'bar'
                ]
            ],
        ]);
        // Requests should be webbased/cgi
        FunctionMock::mock('Miniframe\\Core', 'php_sapi_name', function () {
            return 'cgi';
        });

        // Set the state secret
        $reflection = new \ReflectionProperty(SocialLogin::class, 'stateSecret');
        $reflection->setAccessible(true);
        $reflection->setValue(null, 'foobar');
    }

    /**
     * Tears down after each test
     *
     * @return void
     */
    protected function tearDown(): void
    {
        FunctionMock::releaseAll();
        if (Registry::has(Session::class)) {
            Registry::get(Session::class)->commit();
        }
        Registry::register(Session::class, null);
        Registry::register(SocialLogin::class, null);
    }

    /**
     * Step 1: Redirect to OAuth provider
     *
     * @return void
     */
    public function testAuthenticateStep1(): void
    {
        // Mimics cURL
        setCurlExecMock([$this, 'curlResponse']);

        // Registers middlewares
        $request = new Request(['REQUEST_URI' => '/_SOCIALLOGIN/login/' . $this->providerName]);
        Registry::register(Session::class, new Session($request, $this->config));
        Registry::register(SocialLogin::class, new SocialLogin($request, $this->config));

        // Authenticate, expect redirection to provider
        $provider = new $this->providerFQCN($request, $this->config);
        $this->expectException(RedirectResponse::class);
        $provider->authenticate();
    }

    /**
     * Step 2: Fetch user profile
     *
     * @param string $uniqueKey   The expected unique key.
     * @param string $username    The expected username.
     * @param string $displayName The expected display name.
     * @param string $avatar      The expected avatar.
     *
     * @return void
     */
    public function testAuthenticateStep2(
        string $uniqueKey = '1337',
        string $username = 'foobar',
        string $displayName = 'Foo Bar',
        string $avatar = 'https://foo.bar/baz'
    ): void {
        // Mimics cURL
        setCurlExecMock([$this, 'curlResponse']);

        // Registers middlewares
        $request = new Request(
            ['REQUEST_URI' => '/_SOCIALLOGIN/login/' . $this->providerName],
            ['oauth_token' => 'foobar', 'oauth_verifier' => 'true']
        );
        $session = new Session($request, $this->config);
        Registry::register(Session::class, $session);
        Registry::register(SocialLogin::class, new SocialLogin($request, $this->config));

        // Set initial session
        $session->set('_SOCIALLOGIN', [$this->providerFQCN => ['requestOAuthToken' => 'foobar']]);

        // Authenticate, expect user object
        $provider = new $this->providerFQCN($request, $this->config);
        $user = $provider->authenticate();
        $this->assertEquals($uniqueKey, $user->getUniqueKey());
        $this->assertEquals($username, $user->getUserName());
        $this->assertEquals($displayName, $user->getDisplayName());
        $this->assertEquals($avatar, $user->getAvatar());
        $this->assertEquals($this->providerFQCN, $user->getProviderFQCN());
        $this->assertIsArray($user->getRawData());
    }
}
