<?php

namespace Miniframe\SocialLogin\Provider;

class BitbucketTest extends AbstractAbstractOAuth2ProviderTest
{
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName = 'bitbucket';

    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN = Bitbucket::class;

    /**
     * Tests getLogoSource
     *
     * @return void
     */
    public function testGetLogoSource(): void
    {
        $this->assertStringStartsWith('data:image/svg+xml;base64,', Bitbucket::getLogoSource());
    }

    /**
     * Tests getThemeColor
     *
     * @return void
     */
    public function testGetThemeColor(): void
    {
        $this->assertEquals('rgb(8,76,164)', Bitbucket::getThemeColor());
    }

    /**
     * Simulates a cURL call
     *
     * @param resource $ch       CURL resource handler.
     * @param array    $curlInfo CURL info.
     *
     * @return mixed
     */
    public function curlResponse($ch, array &$curlInfo)
    {
        $curlInfo['http_code'] = 200;
        if ($curlInfo['url'] == 'https://bitbucket.org/site/oauth2/access_token') {
            return json_encode(['access_token' => 'foobar']);
        }
        if ($curlInfo['url'] == 'https://api.bitbucket.org/2.0/user') {
            return json_encode([
                'uuid' => 'foo',
                'username' => 'foo@bar.baz',
                'display_name' => 'Foo Bar',
                'account_status' => $this->accountStatus,
                'links' => ['avatar' => ['href' => 'https://foo.bar.baz/']],
            ]);
        }
    }

    /**
     * Sets the result for cURL requests
     *
     * @var string
     */
    protected $accountStatus = 'active';

    /**
     * Bitbucket test with active user
     *
     * @param string $uniqueKey   The expected unique key.
     * @param string $username    The expected username.
     * @param string $displayName The expected display name.
     * @param string $avatar      The expected avatar.
     *
     * @return void
     */
    public function testAuthenticateStep2(
        string $uniqueKey = 'foo',
        string $username = 'foo@bar.baz',
        string $displayName = 'Foo Bar',
        string $avatar = 'https://foo.bar.baz/'
    ): void {
        $this->accountStatus = 'active';
        parent::testAuthenticateStep2($uniqueKey, $username, $displayName, $avatar);
    }

    /**
     * Bitbucket test with inactive user
     *
     * @return void
     */
    public function testAuthenticateStep2NotVerified(): void
    {
        $this->accountStatus = 'inactive';
        $this->expectException(\RuntimeException::class);
        parent::testAuthenticateStep2();
    }
}
