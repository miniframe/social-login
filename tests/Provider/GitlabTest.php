<?php

namespace Miniframe\SocialLogin\Provider;

class GitlabTest extends AbstractAbstractOAuth2ProviderTest
{
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName = 'gitlab';

    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN = Gitlab::class;

    /**
     * Tests getLogoSource
     *
     * @return void
     */
    public function testGetLogoSource(): void
    {
        $this->assertStringStartsWith('data:image/svg+xml;base64,', Gitlab::getLogoSource());
    }

    /**
     * Tests getThemeColor
     *
     * @return void
     */
    public function testGetThemeColor(): void
    {
        $this->assertEquals('rgba(51,46,107,255)', Gitlab::getThemeColor());
    }

    /**
     * Simulates a cURL call
     *
     * @param resource $ch       CURL resource handler.
     * @param array    $curlInfo CURL info.
     *
     * @return mixed
     */
    public function curlResponse($ch, array &$curlInfo)
    {
        $curlInfo['http_code'] = 200;
        if ($curlInfo['url'] == 'https://gitlab.com/oauth/token') {
            return json_encode(['access_token' => 'foobar']);
        }
        if ($curlInfo['url'] == 'https://gitlab.com/api/v4/user') {
            return json_encode([
                'id' => 'foo',
                'username' => 'foo@bar.baz',
                'name' => 'Foo Bar',
                'avatar_url' => 'https://foo.bar.baz/',
            ]);
        }
    }
}
