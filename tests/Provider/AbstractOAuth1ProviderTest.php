<?php

namespace Miniframe\SocialLogin\Provider;

use Garrcomm\PHPUnitHelpers\FunctionMock;
use Miniframe\Core\Config;
use Miniframe\Core\Registry;
use Miniframe\Core\Request;
use Miniframe\Middleware\Session;
use Miniframe\Response\RedirectResponse;
use Miniframe\SocialLogin\Middleware\SocialLogin;
use PHPUnit\Framework\TestCase;

/**
 * Most of the abstract class is tested by testing the providers themselves,
 * but some cases are not covered by those tests (mostly the error scenarios).
 *
 * This test uses Twitter as provider, but that could be any other OAuth1 provider.
 */
class AbstractOAuth1ProviderTest extends TestCase
{
    /**
     * Dummy config.
     *
     * @var Config
     */
    protected $config;
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName = 'twitter';
    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN = Twitter::class;

    /**
     * This method is called before each test.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        if (!$this->providerName || !$this->providerFQCN) {
            throw new \RuntimeException('Provider name must be specified');
        }

        // Defines a generic config
        $this->config = Config::__set_state([
            'configFolder' => __DIR__,
            'projectFolder' => __DIR__,
            'data' => [
                'framework' => ['base_href' => '/'],
                'sociallogin' => ['state_secret' => 'foobar', 'providers' => [$this->providerName]],
                'sociallogin-' . $this->providerName => [
                    'consumer_key' => 'foo',
                    'consumer_secret' => 'bar'
                ]
            ],
        ]);
        // Requests should be webbased/cgi
        FunctionMock::mock('Miniframe\\Core', 'php_sapi_name', function () {
            return 'cgi';
        });

        // Set the state secret
        $reflection = new \ReflectionProperty(SocialLogin::class, 'stateSecret');
        $reflection->setAccessible(true);
        $reflection->setValue(null, 'foobar');
    }

    /**
     * Tears down after each test
     *
     * @return void
     */
    protected function tearDown(): void
    {
        FunctionMock::releaseAll();
        if (Registry::has(Session::class)) {
            Registry::get(Session::class)->commit();
        }
        Registry::register(Session::class, null);
        Registry::register(SocialLogin::class, null);
    }

    /**
     * Tests authenticating with a state in the request
     *
     * @return void
     */
    public function testStateFromRequest(): void
    {
        // Mimics cURL
        setCurlExecMock([new TwitterTest(), 'curlResponse']);

        // Registers middlewares
        $request = new Request(
            ['REQUEST_URI' => '/_SOCIALLOGIN/login/' . $this->providerName],
            ['state' => SocialLogin::generateState(['redirectUrl' => '/'])]
        );
        $session = new Session($request, $this->config);
        Registry::register(Session::class, $session);
        Registry::register(SocialLogin::class, new SocialLogin($request, $this->config));

        // Authenticate, expect redirection to provider
        $provider = new $this->providerFQCN($request, $this->config);
        try {
            $provider->authenticate();
            $this->assertTrue(false, 'Expected RedirectResponse');
        } catch (RedirectResponse $response) {
            $sessionData = $session->get('_SOCIALLOGIN');
            $sessionState = $sessionData[Twitter::class]['userState'];
            $this->assertEquals($request->getRequest('state'), $sessionState);
        }
    }

    /**
     * Tests authenticating with a state in the post body
     *
     * @return void
     */
    public function testStateFromPost(): void
    {
        // Mimics cURL
        setCurlExecMock([new TwitterTest(), 'curlResponse']);

        // Registers middlewares
        $request = new Request(
            ['REQUEST_URI' => '/_SOCIALLOGIN/login/' . $this->providerName, 'REQUEST_METHOD' => 'POST'],
            [],
            ['state' => SocialLogin::generateState(['redirectUrl' => '/'])]
        );
        $session = new Session($request, $this->config);
        Registry::register(Session::class, $session);
        Registry::register(SocialLogin::class, new SocialLogin($request, $this->config));

        // Authenticate, expect redirection to provider
        $provider = new $this->providerFQCN($request, $this->config);
        try {
            $provider->authenticate();
            $this->assertTrue(false, 'Expected RedirectResponse');
        } catch (RedirectResponse $response) {
            $sessionData = $session->get('_SOCIALLOGIN');
            $sessionState = $sessionData[Twitter::class]['userState'];
            $this->assertEquals($request->getPost('state'), $sessionState);
        }
    }

    /**
     * Tests redirect after full authentication
     *
     * @return void
     */
    public function testRedirectAfterStep2(): void
    {
        // Mimics cURL
        setCurlExecMock([new TwitterTest(), 'curlResponse']);

        // Registers middlewares
        $request = new Request(
            ['REQUEST_URI' => '/_SOCIALLOGIN/login/' . $this->providerName, 'REQUEST_METHOD' => 'POST'],
            ['oauth_token' => 'foobar', 'oauth_verifier' => 'true']
        );
        $session = new Session($request, $this->config);
        Registry::register(Session::class, $session);
        Registry::register(SocialLogin::class, new SocialLogin($request, $this->config));

        // Set initial session
        $session->set('_SOCIALLOGIN', [$this->providerFQCN => [
            'requestOAuthToken' => 'foobar',
            'userState' => SocialLogin::generateState(['redirectUrl' => '/']),
        ]]);

        // Authenticate, expect redirect
        $provider = new $this->providerFQCN($request, $this->config);
        $this->expectException(RedirectResponse::class);
        $provider->authenticate();
    }

    /**
     * Tests redirect after full authentication
     *
     * @return void
     */
    public function testInvalidRequestTokenResponse(): void
    {
        // Mimics cURL
        setCurlExecMock(function (/* resource */ $ch, array &$curlInfo) {
            $curlInfo['http_code'] = 200;
            if ($curlInfo['url'] == 'https://api.twitter.com/oauth/request_token') {
                $curlInfo['content_type'] = 'text/html';
                return http_build_query(['no_oauth_token' => 'foo', 'oauth_token_secret' => 'bar']);
            }
        });

        // Registers middlewares
        $request = new Request(['REQUEST_URI' => '/_SOCIALLOGIN/login/' . $this->providerName]);
        Registry::register(Session::class, new Session($request, $this->config));
        Registry::register(SocialLogin::class, new SocialLogin($request, $this->config));

        // Authenticate, expect exception
        $provider = new $this->providerFQCN($request, $this->config);
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Request Token failed: No oauth_token returned');
        $provider->authenticate();
    }

    /**
     * Tests redirect after full authentication
     *
     * @return void
     */
    public function testInvalidAccessTokenResponse(): void
    {
        // Mimics cURL
        setCurlExecMock(function (/* resource */ $ch, array &$curlInfo) {
            $curlInfo['http_code'] = 200;
            if ($curlInfo['url'] == 'https://api.twitter.com/oauth/access_token') {
                $curlInfo['content_type'] = 'text/html';
                return http_build_query(['no_oauth_token' => 'foo', 'oauth_token_secret' => 'bar']);
            }
        });

        // Registers middlewares
        $request = new Request(
            ['REQUEST_URI' => '/_SOCIALLOGIN/login/' . $this->providerName],
            ['oauth_token' => 'foobar', 'oauth_verifier' => 'true']
        );
        $session = new Session($request, $this->config);
        Registry::register(Session::class, $session);
        Registry::register(SocialLogin::class, new SocialLogin($request, $this->config));

        // Set initial session
        $session->set('_SOCIALLOGIN', [$this->providerFQCN => ['requestOAuthToken' => 'foobar']]);

        // Authenticate, expect exception
        $provider = new $this->providerFQCN($request, $this->config);
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Request Token failed: No oauth_token returned');
        $provider->authenticate();
    }
}
