<?php

namespace Miniframe\SocialLogin\Provider;

class AtlassianTest extends AbstractAbstractOAuth2ProviderTest
{
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName = 'atlassian';

    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN = Atlassian::class;

    /**
     * Tests getLogoSource
     *
     * @return void
     */
    public function testGetLogoSource(): void
    {
        $this->assertStringStartsWith('data:image/svg+xml;base64,', Atlassian::getLogoSource());
    }

    /**
     * Tests getThemeColor
     *
     * @return void
     */
    public function testGetThemeColor(): void
    {
        $this->assertEquals('rgba(16,76,180,255)', Atlassian::getThemeColor());
    }

    /**
     * Simulates a cURL call
     *
     * @param resource $ch       CURL resource handler.
     * @param array    $curlInfo CURL info.
     *
     * @return mixed
     */
    public function curlResponse($ch, array &$curlInfo)
    {
        $curlInfo['http_code'] = 200;
        if ($curlInfo['url'] == 'https://auth.atlassian.com/oauth/token') {
            return json_encode(['access_token' => 'foobar']);
        }
        if ($curlInfo['url'] == 'https://api.atlassian.com/me') {
            return json_encode([
                'email_verified' => $this->userVerified,
                'account_id' => 'foo',
                'email' => 'foo@bar.baz',
                'name' => 'Foo Bar',
                'picture' => 'https://foo.bar.baz/'
            ]);
        }
    }

    /**
     * Sets the result for cURL requests
     *
     * @var bool
     */
    protected $userVerified = true;

    /**
     * Atlassian test with verified user
     *
     * @param string $uniqueKey   The expected unique key.
     * @param string $username    The expected username.
     * @param string $displayName The expected display name.
     * @param string $avatar      The expected avatar.
     *
     * @return void
     */
    public function testAuthenticateStep2(
        string $uniqueKey = 'foo',
        string $username = 'foo@bar.baz',
        string $displayName = 'Foo Bar',
        string $avatar = 'https://foo.bar.baz/'
    ): void {
        $this->userVerified = true;
        parent::testAuthenticateStep2($uniqueKey, $username, $displayName, $avatar);
    }

    /**
     * Atlassian test with unverified user
     *
     * @return void
     */
    public function testAuthenticateStep2NotVerified(): void
    {
        $this->userVerified = false;
        $this->expectException(\RuntimeException::class);
        parent::testAuthenticateStep2();
    }
}
