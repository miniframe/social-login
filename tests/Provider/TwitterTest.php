<?php

namespace Miniframe\SocialLogin\Provider;

class TwitterTest extends AbstractAbstractOAuth1ProviderTest
{
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName = 'twitter';

    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN = Twitter::class;

    /**
     * Tests getLogoSource
     *
     * @return void
     */
    public function testGetLogoSource(): void
    {
        $this->assertStringStartsWith('data:image/svg+xml;base64,', Twitter::getLogoSource());
    }

    /**
     * Tests getThemeColor
     *
     * @return void
     */
    public function testGetThemeColor(): void
    {
        $this->assertEquals('rgba(30,41,51,255)', Twitter::getThemeColor());
    }

    /**
     * Simulates a cURL call
     *
     * @param resource $ch       CURL resource handler.
     * @param array    $curlInfo CURL info.
     *
     * @return mixed
     */
    public function curlResponse($ch, array &$curlInfo)
    {
        $curlInfo['http_code'] = 200;
        if ($curlInfo['url'] == 'https://api.twitter.com/oauth/request_token') {
            $curlInfo['content_type'] = 'text/html';
            return http_build_query(['oauth_token' => 'foo', 'oauth_token_secret' => 'bar']);
        } elseif ($curlInfo['url'] == 'https://api.twitter.com/oauth/authorize') {
            return http_build_query(['oauth_token' => 'foo']);
        } elseif ($curlInfo['url'] == 'https://api.twitter.com/oauth/access_token') {
            $curlInfo['content_type'] = 'text/html';
            return http_build_query([
                'oauth_token' => 'foo',
                'oauth_token_secret' => 'bar',
                'screen_name' => 'foobar',
                'user_id' => '1337',
            ]);
        } elseif (
            $curlInfo['url'] == 'https://api.twitter.com/1.1/users/show.json?screen_name=foobar&oauth_token=foo'
        ) {
            return json_encode([
                'name' => 'Foo Bar',
                'profile_image_url_https' => 'https://foo.bar/baz'
            ]);
        }
    }
}
