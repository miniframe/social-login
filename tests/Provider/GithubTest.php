<?php

namespace Miniframe\SocialLogin\Provider;

class GithubTest extends AbstractAbstractOAuth2ProviderTest
{
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName = 'github';

    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN = Github::class;

    /**
     * Tests getLogoSource
     *
     * @return void
     */
    public function testGetLogoSource(): void
    {
        $this->assertStringStartsWith('data:image/svg+xml;base64,', Github::getLogoSource());
    }

    /**
     * Tests getThemeColor
     *
     * @return void
     */
    public function testGetThemeColor(): void
    {
        $this->assertEquals('rgba(28,33,39,255)', Github::getThemeColor());
    }

    /**
     * Simulates a cURL call
     *
     * @param resource $ch       CURL resource handler.
     * @param array    $curlInfo CURL info.
     *
     * @return mixed
     */
    public function curlResponse($ch, array &$curlInfo)
    {
        $curlInfo['http_code'] = 200;
        if ($curlInfo['url'] == 'https://github.com/login/oauth/access_token') {
            $curlInfo['content_type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
            return http_build_query(['access_token' => 'foobar']);
        }
        if ($curlInfo['url'] == 'https://api.github.com/user') {
            return json_encode([
                'node_id' => 'foo',
                'login' => 'foo@bar.baz',
                'name' => 'Foo Bar',
                'avatar_url' => 'https://foo.bar.baz/',
            ]);
        }
    }
}
