<?php

namespace Miniframe\SocialLogin\Provider;

class InstagramTest extends AbstractAbstractOAuth2ProviderTest
{
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName = 'instagram';

    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN = Instagram::class;

    /**
     * Tests getLogoSource
     *
     * @return void
     */
    public function testGetLogoSource(): void
    {
        $this->assertStringStartsWith('data:image/svg+xml;base64,', Instagram::getLogoSource());
    }

    /**
     * Tests getThemeColor
     *
     * @return void
     */
    public function testGetThemeColor(): void
    {
        $this->assertEquals('rgb(193,53,132)', Instagram::getThemeColor());
    }

    /**
     * Simulates a cURL call
     *
     * @param resource $ch       CURL resource handler.
     * @param array    $curlInfo CURL info.
     *
     * @return mixed
     */
    public function curlResponse($ch, array &$curlInfo)
    {
        $curlInfo['http_code'] = 200;
        if ($curlInfo['url'] == 'https://api.instagram.com/oauth/access_token') {
            return json_encode(['access_token' => 'foobar']);
        }
        if ($curlInfo['url'] == 'https://graph.instagram.com/v11.0/me?fields=id,username') {
            return json_encode([
                'id' => 'foo',
                'username' => 'foobarbaz',
            ]);
        }
        if ($curlInfo['url'] == 'https://www.instagram.com/foobarbaz/?__a=1' && $this->notFoundError) {
            $curlInfo['http_code'] = 404;
            return 'Not found page';
        }
        if ($curlInfo['url'] == 'https://www.instagram.com/foobarbaz/?__a=1' && !$this->rateLimited) {
            return json_encode([
                'graphql' => [
                    'user' => [
                        'profile_pic_url' => 'https://foo.bar.baz/image.jpg',
                        'full_name' => 'Foo Bar',
                    ]
                ]
            ]);
        }
        if ($curlInfo['url'] == 'https://www.instagram.com/foobarbaz/?__a=1' && $this->rateLimited) {
            $curlInfo['http_code'] = 302;
            return 'Login page';
        }
        if ($curlInfo['url'] == 'https://foo.bar.baz/image.jpg') {
            $curlInfo['content_type'] = 'image/jpeg';
            return 'foobar';
        }
    }

    /**
     * Rate limit simulation
     *
     * @var bool
     */
    protected $rateLimited = false;

    /**
     * Not found simulation
     *
     * @var bool
     */
    protected $notFoundError = false;

    /**
     * Instagram doesn't have email addresses as username
     *
     * @param string $uniqueKey   The expected unique key.
     * @param string $username    The expected username.
     * @param string $displayName The expected display name.
     * @param string $avatar      The expected avatar.
     *
     * @return void
     */
    public function testAuthenticateStep2(
        string $uniqueKey = 'foo',
        string $username = 'foo@bar.baz',
        string $displayName = 'Foo Bar',
        string $avatar = 'https://foo.bar.baz/'
    ): void {
        $username = 'foobarbaz';
        $avatar = 'data:image/jpeg;base64,Zm9vYmFy';
        $this->rateLimited = false;
        $this->notFoundError = false;
        parent::testAuthenticateStep2($uniqueKey, $username, $displayName, $avatar);
    }

    /**
     * Tests what happens when the rate limit appears
     *
     * @return void
     */
    public function testAuthenticateStep2WithRateLimit(): void
    {
        $displayName = 'foobarbaz';
        $avatar = 'data:image/svg+xml;base64,'
            . base64_encode(file_get_contents(__DIR__ . '/../../templates/unknown-user.svg'));
        $this->rateLimited = true;
        $this->notFoundError = false;
        parent::testAuthenticateStep2('foo', 'foobarbaz', $displayName, $avatar);
    }

    /**
     * Tests what happens when another error happens
     *
     * @return void
     */
    public function testAuthenticateStep2WithStrangeError(): void
    {
        $this->rateLimited = false;
        $this->notFoundError = true;
        $this->expectException(\RuntimeException::class);
        parent::testAuthenticateStep2();
    }
}
