<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\Core\Request;
use Miniframe\SocialLogin\Middleware\SocialLogin;

class GoogleTest extends AbstractAbstractOAuth2ProviderTest
{
    /**
     * Short name of the provider
     *
     * @var string
     */
    protected $providerName = 'google';

    /**
     * Fully qualified classname of the provider
     *
     * @var string
     */
    protected $providerFQCN = Google::class;

    /**
     * Set to true to add a failure in the response
     *
     * @var bool
     */
    protected $failureNotVerifiedResponse = false;

    /**
     * This method is called before each test.
     *
     * @return void
     */
    public function setUp(): void
    {
        $this->failureNotVerifiedResponse = false;
        parent::setUp();
    }

    /**
     * Test with an unverified mail address
     *
     * @return void
     */
    public function testAuthenticateStep2UnverifiedMail(): void
    {
        $state = SocialLogin::generateState(['redirectUrl' => '/']);
        $code = 'foobar';
        $this->failureNotVerifiedResponse = true;

        setCurlExecMock([$this, 'curlResponse']);

        $request = new Request(['REQUEST_URI' => '/?state=' . rawurlencode($state) . '&code=' . rawurlencode($code)], [
            'state' => $state,
            'code' => $code
        ]);
        $provider = new $this->providerFQCN($request, $this->config);
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Email address not verified');
        $user = $provider->authenticate();
    }

    /**
     * Tests getLogoSource
     *
     * @return void
     */
    public function testGetLogoSource(): void
    {
        $this->assertStringStartsWith('data:image/svg+xml;base64,', Google::getLogoSource());
    }

    /**
     * Tests getThemeColor
     *
     * @return void
     */
    public function testGetThemeColor(): void
    {
        $this->assertEquals('#d3a300', Google::getThemeColor());
    }

    /**
     * Simulates a cURL call
     *
     * @param resource $ch       CURL resource handler.
     * @param array    $curlInfo CURL info.
     *
     * @return mixed
     */
    public function curlResponse($ch, array &$curlInfo)
    {
        $curlInfo['http_code'] = 200;
        if ($curlInfo['url'] == 'https://oauth2.googleapis.com/token') {
            return json_encode(['access_token' => 'foobar']);
        }
        if ($curlInfo['url'] == 'https://www.googleapis.com/oauth2/v1/userinfo?alt=json') {
            $profile = [
                'id' => 'foo',
                'email' => 'foo@bar.baz',
                'name' => 'Foo Bar',
                'picture' => 'https://foo.bar.baz/',
            ];
            if ($this->failureNotVerifiedResponse) {
                $profile['verified_email'] = false;
            }
            return json_encode($profile);
        }
    }
}
