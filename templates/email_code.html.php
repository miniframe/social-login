<?php

/**
 * This template contains the social login page
 */

/* @var $socialLoginPrefixUrl string */
/* @var $publicHref string|null */
/* @var $state string */
/* @var $email string */
/* @var $error string|null */
/* @var $darkmode string */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Login</title>
    <style>
        <?= file_get_contents(__DIR__ . '/css/main.css'); ?>
        <?php if ($darkmode == 'on') : ?>
            <?= file_get_contents(__DIR__ . '/css/darkmode.css'); ?>
        <?php elseif ($darkmode == 'auto') : ?>
            @media (prefers-color-scheme: dark) {
                <?= file_get_contents(__DIR__ . '/css/darkmode.css'); ?>
            }
        <?php endif; ?>
    </style>
</head>
<body>
<div class="container h-100">
    <div class="row align-items-center h-100">
        <div class="col-6 mx-auto">

            <div class="card">
                <h5 class="card-header">
                    Login
                    <?php if ($publicHref !== null) : ?>
                        <a href="<?= htmlspecialchars($publicHref) ?>" type="button" class="close" title="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
                    <?php endif; ?>
                </h5>
                <div class="card-body">
                    <?php if ($error) : ?>
                        <div class="alert alert-danger" role="alert"><?= htmlspecialchars($error) ?></div>
                    <?php endif; ?>
                    <h5 class="card-title">Sign in with an email address</h5>
                    <form method="post" action="<?= htmlspecialchars($socialLoginPrefixUrl) ?>email/signIn" autocomplete="off">
                        <input type="hidden" name="state" value="<?= htmlspecialchars($state) ?>">
                        <div class="form-group row">
                            <label for="email" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" readonly id="email" aria-describedby="emailHelp" value="<?= htmlspecialchars($email) ?>">
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else and only send a single email with a one-time login code.</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-2 col-form-label">Code</label>
                            <div class="col-sm-10">
                                <input name="code" type="text" class="form-control" id="code" aria-describedby="codeHelp" placeholder="Code" autocomplete="off">
                                <small id="codeHelp" class="form-text text-muted">A code is sent to the email address <?= htmlspecialchars($email) ?> which is valid for 10 minutes.</small>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
