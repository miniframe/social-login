<?php

namespace Miniframe\SocialLogin\Middleware;

use Miniframe\Core\AbstractMiddleware;
use Miniframe\Core\Config;
use Miniframe\Core\Registry;
use Miniframe\Core\Request;
use Miniframe\Middleware\Session;
use Miniframe\Response\RedirectResponse;
use Miniframe\SocialLogin\Controller\Email;
use Miniframe\SocialLogin\Controller\Login;
use Miniframe\SocialLogin\Model\User;
use Miniframe\SocialLogin\Provider\ProviderInterface;

class SocialLogin extends AbstractMiddleware
{
    /**
     * Base URL for the Social Login pages
     *
     * @var string
     */
    protected $socialLoginPrefixUrl;

    /**
     * Reference to the Session middleware
     *
     * @var Session
     */
    protected $session;

    /**
     * List of all enabled providers (FQCNs)
     *
     * @var array
     */
    protected $providers = array();

    /**
     * Initiates the Social Login middleware
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the config.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);
        $this->parseConfig();

        // We require sessions to store the user login
        if (!Registry::has(Session::class)) {
            throw new \RuntimeException(
                'The SocialLogin extension requires the Session middleware'
                . ' (https://bitbucket.org/miniframe/core/src/v1/src/Middleware/Session.md)'
            );
        }
        $this->session = Registry::get(Session::class);

        // Determine URLs
        $baseHref = $config->get('framework', 'base_href');
        $this->socialLoginPrefixUrl = $baseHref . '_SOCIALLOGIN/';

        // Validate the current URL, if access is granted
        $fullPath = '/' . implode('/', $request->getPath());

        if ($this->isLoginRequired($baseHref, $fullPath)) {
            // Validate session, maybe throw a new RedirectResponse() to a login page.
            $state = [
                'redirectUrl' => $fullPath, // After logging in, redirect back to this URL
            ];
            throw new RedirectResponse(
                $this->socialLoginPrefixUrl . 'login?state=' . rawurlencode(SocialLogin::generateState($state))
            );
        }
    }

    /**
     * Verify if a login is required
     *
     * @param string $baseHref The base href.
     * @param string $fullPath The full path.
     *
     * @return boolean
     */
    protected function isLoginRequired(string $baseHref, string $fullPath): bool
    {
        $sessionData = $this->session->get('_SOCIALLOGIN');

        // Exclusions
        $exclusionBase = $this->request->isShellRequest() ? '/' : $baseHref;
        $excludeList = $this->config->has('sociallogin', 'exclude') ? $this->config->get('sociallogin', 'exclude') : [];
        foreach ($excludeList as $excludePath) {
            $regex = '/^'
                . str_replace(['?', '\\*'], ['.', '.*?'], preg_quote($exclusionBase . ltrim($excludePath, '/'), '/'))
                . '$/';
            if (preg_match($regex, $fullPath)) {
                return false;
            }
        }

        // Requests starting with _SOCIALLOGIN are always open; they're for authentication.
        if (substr($fullPath, 0, strlen($this->socialLoginPrefixUrl)) === $this->socialLoginPrefixUrl) {
            return false;
        }
        // We're logged in, no authentication required
        if (isset($sessionData['loggedin']) && $sessionData['loggedin'] === true) {
            return false;
        }

        return true;
    }

    /**
     * Logs in with a specific user
     *
     * @param User $user The user object.
     *
     * @return void
     */
    public function login(User $user): void
    {
        $sessionData = $this->session->get('_SOCIALLOGIN');
        $sessionData['loggedin'] = true;
        $sessionData['user'] = $user;
        $this->session->set('_SOCIALLOGIN', $sessionData);
    }

    /**
     * Logs out the current user
     *
     * @return void
     */
    public function logout(): void
    {
        $sessionData = $this->session->get('_SOCIALLOGIN');
        $sessionData['loggedin'] = false;
        unset($sessionData['user']);
        $this->session->set('_SOCIALLOGIN', $sessionData);
    }

    /**
     * Returns the user of the user that's currently logged in.
     *
     * @return User|null
     */
    public function getUser(): ?User
    {
        $sessionData = $this->session->get('_SOCIALLOGIN');
        if (!isset($sessionData['loggedin']) || $sessionData['loggedin'] !== true) {
            return null;
        }
        return $sessionData['user'] ?? null;
    }

    /**
     * Returns the URL to the logout page
     *
     * @return string
     */
    public function getLogoutHref(): string
    {
        $state = ['redirectUrl' => '/' . implode('/', $this->request->getPath())];
        return $this->socialLoginPrefixUrl . 'logout?state=' . rawurlencode(SocialLogin::generateState($state));
    }

    /**
     * Adds the Social Login routes
     *
     * @return array
     */
    public function getRouters(): array
    {
        return [function (): ?callable {
            $comparePath = explode('/', trim($this->socialLoginPrefixUrl, '/'));
            foreach ($comparePath as $index => $path) {
                if ($this->request->getPath($index) != $path) {
                    return null;
                }
            }

            // What's the subcommand?
            $subCommand = $this->request->getPath(++$index);

            // Email provider
            if ($subCommand == 'email') {
                $action = $this->request->getPath(++$index);
                if (!method_exists(Email::class, $action)) {
                    return null;
                }
                return [
                    new Email($this->request, $this->config, $this, $this->socialLoginPrefixUrl, $this->session),
                    $action
                ];
            }

            // Is the subcommand valid in the Login controller?
            if ($subCommand && !method_exists(Login::class, $subCommand)) {
                return null;
            }

            // Is a provider specified?
            $providerName = $this->request->getPath(++$index);

            // Regular provider
            if ($providerName) {
                $providerFQCN = $this->getProvider($providerName);
                $provider = new $providerFQCN($this->request, $this->config); /* @var $provider ProviderInterface */
            }

            return [
                new Login(
                    $this->request,
                    $this->config,
                    $this,
                    $this->socialLoginPrefixUrl,
                    $this->providers,
                    $provider ?? null
                ),
                $subCommand ?? 'login'
            ];
        }];
    }

    /**
     * Parses the config and populates the 'providers' array
     *
     * @return void
     */
    protected function parseConfig(): void
    {
        // Fetch state secret
        static::$stateSecret = $this->config->get('sociallogin', 'state_secret');

        // Fetch provider configuration
        $providers = $this->config->get('sociallogin', 'providers');
        if (!is_array($providers)) {
            throw new \RuntimeException(
                'The providers config value should be a list. (see '
                . 'https://bitbucket.org/miniframe/miniframe-social-login/src/v1/README.md for more)'
            );
        }

        // Do the providers exist?
        foreach ($providers as $provider) {
            $provider = strtolower($provider);
            if (class_exists('App\\SocialLogin\\Provider\\' . ucfirst($provider))) {
                $this->providers[$provider] = 'App\\SocialLogin\\Provider\\' . ucfirst($provider);
            } elseif (class_exists('Miniframe\\SocialLogin\\Provider\\' . ucfirst($provider))) {
                $this->providers[$provider] = 'Miniframe\\SocialLogin\\Provider\\' . ucfirst($provider);
            } else {
                throw new \RuntimeException(
                    'Provider "' . $provider . '" not found in namespaces '
                    . 'Miniframe\\SocialLogin\\Provider and'
                    . 'App\\SocialLogin\\Provider.'
                );
            }
        }

        // Do the providers implement the correct interface?
        foreach ($this->providers as $provider) {
            if (!in_array(ProviderInterface::class, class_implements($provider))) {
                throw new \RuntimeException($provider . ' does not implement ' . ProviderInterface::class);
            }
        }

        // Is autologin legal?
        if (
            $this->config->has('sociallogin', 'autologin')
            && $this->config->get('sociallogin', 'autologin') == true
            && count($providers) > 1
        ) {
            throw new \InvalidArgumentException('Autologin can\'t be enabled when there is more then 1 provider');
        }
    }

    /**
     * Returns a provider fully qualified class name by its config name
     *
     * @param string $provider Config name of the provider.
     *
     * @return string
     */
    protected function getProvider(string $provider): string
    {
        $provider = strtolower($provider);
        if (!isset($this->providers[$provider])) {
            throw new \RuntimeException('Provider ' . $provider . ' not found');
        }
        return $this->providers[$provider];
    }

    /**
     * Should contain the state secret
     *
     * @var string|null
     */
    protected static $stateSecret;

    /**
     * Parses a state string
     *
     * @param string|null $data The state as base64-encoded, json-encoded string.
     *
     * @return array
     */
    public static function parseState(?string $data): array
    {
        if (!static::$stateSecret) {
            throw new \RuntimeException('No state secret configured. Please configure a state secret.');
        }
        if (!$data) {
            return array();
        }

        // Decodes content, if it's an empty array, don't validate.
        $decoded = json_decode(base64_decode($data), true, 2, JSON_THROW_ON_ERROR);
        if (!$decoded) {
            return array();
        }

        // Validate!
        $validateArray = $decoded;
        unset($validateArray['validate']);
        $validateString = sha1(static::$stateSecret . serialize($validateArray));

        if (!isset($decoded['validate']) || $validateString !== $decoded['validate']) {
            throw new \UnexpectedValueException('The state looks tampered');
        }

        return $decoded;
    }

    /**
     * Converts an array to a base64-encoded, json-encoded string.
     *
     * @param array $data The data array.
     *
     * @return string
     */
    public static function generateState(array $data): string
    {
        if (!static::$stateSecret) {
            throw new \RuntimeException('No state secret configured. Please configure a state secret.');
        }

        $validateArray = $data;
        if (isset($validateArray['validate'])) {
            unset($validateArray['validate']);
        }
        $data['validate'] = sha1(static::$stateSecret . serialize($validateArray));

        return base64_encode(json_encode($data, JSON_THROW_ON_ERROR));
    }
}
