<?php

namespace Miniframe\SocialLogin\Model;

class User
{
    /**
     * The primary key from the social login service.
     *
     * @var string
     */
    protected $uniqueKey;
    /**
     * The username.
     *
     * @var string
     */
    protected $userName;
    /**
     * The display name.
     *
     * @var string
     */
    protected $displayName;
    /**
     * Avatar URL (can be a gravatar URL).
     *
     * @var string
     */
    protected $avatar;
    /**
     * The fully qualified classname of the login provider.
     *
     * @var string
     */
    protected $providerFQCN;
    /**
     * Raw data.
     *
     * @var array
     */
    protected $rawData;

    /**
     * Creates a new User instance
     *
     * @param string $uniqueKey    The primary key from the social login service.
     * @param string $userName     The username.
     * @param string $displayName  The display name.
     * @param string $avatar       Avatar URL (can be a gravatar URL).
     * @param string $providerFQCN The fully qualified classname of the login provider.
     * @param array  $rawData      Raw data.
     */
    public function __construct(
        string $uniqueKey,
        string $userName,
        string $displayName,
        string $avatar,
        string $providerFQCN,
        array $rawData
    ) {
        $this->uniqueKey = $uniqueKey;
        $this->userName = $userName;
        $this->displayName = $displayName;
        $this->avatar = $avatar;
        $this->providerFQCN = $providerFQCN;
        $this->rawData = $rawData;
    }

    /**
     * Returns the primary key from the social login service.
     *
     * @return string
     */
    public function getUniqueKey(): string
    {
        return $this->uniqueKey;
    }

    /**
     * Returns the username.
     *
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * Returns the display name.
     *
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * Returns the avatar URL (can be a gravatar URL).
     *
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->avatar;
    }

    /**
     * Returns the fully qualified classname of the login provider.
     *
     * @return string
     */
    public function getProviderFQCN(): string
    {
        return $this->providerFQCN;
    }

    /**
     * Returns the raw data.
     *
     * @return array
     */
    public function getRawData(): array
    {
        return $this->rawData;
    }

    /**
     * Returns a user object with a specific state (makes this object serializable with var_export)
     *
     * @param array $properties The list of properties.
     *
     * @return User
     */
    public static function __set_state(array $properties): User
    {
        return new User(
            $properties['uniqueKey'],
            $properties['userName'],
            $properties['displayName'],
            $properties['avatar'],
            $properties['providerFQCN'],
            $properties['rawData']
        );
    }
}
