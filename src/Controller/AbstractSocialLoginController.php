<?php

namespace Miniframe\SocialLogin\Controller;

use Miniframe\Core\AbstractController;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\SocialLogin\Middleware\SocialLogin;

abstract class AbstractSocialLoginController extends AbstractController
{
    /**
     * Config value of dark mode
     *
     * @var string
     */
    protected $darkmode = 'off';
    /**
     * Reference to the Social Login middleware.
     *
     * @var SocialLogin
     */
    protected $socialLogin;
    /**
     * Base URL for the Social Login pages.
     *
     * @var string
     */
    protected $socialLoginPrefixUrl;

    /**
     * Initializes a Social Login controller
     *
     * @param Request     $request              Reference to the Request object.
     * @param Config      $config               Reference to the Config object.
     * @param SocialLogin $socialLogin          Reference to the Social Login middleware.
     * @param string      $socialLoginPrefixUrl Base URL for the Social Login pages.
     */
    public function __construct(
        Request $request,
        Config $config,
        SocialLogin $socialLogin,
        string $socialLoginPrefixUrl
    ) {
        parent::__construct($request, $config);
        $this->socialLogin = $socialLogin;
        $this->socialLoginPrefixUrl = $socialLoginPrefixUrl;

        // Dark mode configuration
        if ($config->has('sociallogin', 'darkmode')) {
            if (
                $config->get('sociallogin', 'darkmode') === false
                || strtolower($config->get('sociallogin', 'darkmode')) == 'off'
            ) {
                $this->darkmode = 'off';
            } elseif (
                $config->get('sociallogin', 'darkmode') === true
                || strtolower($config->get('sociallogin', 'darkmode')) == 'on'
            ) {
                $this->darkmode = 'on';
            } elseif (
                strtolower($config->get('sociallogin', 'darkmode')) == 'auto'
            ) {
                $this->darkmode = 'auto';
            } else {
                throw new \RuntimeException('Invalid Dark Mode flag: ' . $config->get('sociallogin', 'darkmode'));
            }
        }
    }
}
