<?php

namespace Miniframe\SocialLogin\Controller;

use Miniframe\Core\AbstractController;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Middleware\Session;
use Miniframe\Response\PhpResponse;
use Miniframe\Response\RedirectResponse;
use Miniframe\SocialLogin\Middleware\SocialLogin;
use Miniframe\SocialLogin\Provider\Email as EmailProvider;

class Email extends AbstractSocialLoginController
{
    /**
     * Reference to the user session.
     *
     * @var Session
     */
    protected $session;
    /**
     * Reference to the Email Service
     *
     * @var EmailProvider
     */
    protected $emailService;

    /**
     * Initializes the Email controller
     *
     * @param Request     $request              Reference to the Request object.
     * @param Config      $config               Reference to the Config object.
     * @param SocialLogin $socialLogin          Reference to the Social Login middleware.
     * @param string      $socialLoginPrefixUrl Base URL for the Social Login pages.
     * @param Session     $session              Reference to the user session.
     */
    public function __construct(
        Request $request,
        Config $config,
        SocialLogin $socialLogin,
        string $socialLoginPrefixUrl,
        Session $session
    ) {
        parent::__construct($request, $config, $socialLogin, $socialLoginPrefixUrl);
        $this->session = $session;
        $this->emailService = new EmailProvider(
            $this->request,
            $this->config,
            $this->session,
            $this->socialLoginPrefixUrl
        );

        // Extra security check; is emailing enabled?
        if (!preg_grep('/^email$/i', $this->config->get('sociallogin', 'providers'))) {
            throw new \RuntimeException('Email provider is not enabled');
        }
    }

    /**
     * Sends the code and redirects to the next step, or go back to the login page with an error message.
     *
     * @return Response
     */
    public function sendCode(): Response
    {
        $state = SocialLogin::parseState($this->request->getRequest('state') ?? $this->request->getPost('state'));
        try {
            $this->emailService->authenticate();
        } catch (\InvalidArgumentException $exception) {
            $state['error'] = $exception->getMessage();
        }
        return new RedirectResponse(
            $this->socialLoginPrefixUrl . 'login?state=' . rawurlencode(SocialLogin::generateState($state))
        );
    }

    /**
     * Shows the page where the end-user can enter the code.
     *
     * @return Response
     */
    public function code(): Response
    {
        // Define the public href for the "Close" button
        $publicHref = $this->config->has('sociallogin', 'public_href')
            ? $this->config->get('sociallogin', 'public_href') : null;

        // Do we have an error?
        $state = SocialLogin::parseState($this->request->getRequest('state') ?? $this->request->getPost('state'));
        $error = $state['error'] ?? null;
        $state['error'] = null;

        // Return template
        return new PhpResponse(__DIR__ . '/../../templates/email_code.html.php', [
            'socialLoginPrefixUrl' => $this->socialLoginPrefixUrl,
            'publicHref' => $publicHref,
            'email' => $this->session->get('_SOCIALLOGIN')['email']['address'],
            'state' => SocialLogin::generateState($state),
            'error' => $error,
            'darkmode' => $this->darkmode,
        ]);
    }

    /**
     * Signs in the user, or go back to the code page with an error message.
     *
     * @return Response
     */
    public function signIn(): Response
    {
        $state = SocialLogin::parseState($this->request->getRequest('state') ?? $this->request->getPost('state'));
        try {
            $this->socialLogin->login($this->emailService->authenticate());
            // Redirect to original page
            return new RedirectResponse($state['redirectUrl'] ?? $this->config->get('framework', 'base_href'));
        } catch (\InvalidArgumentException $exception) {
            $state['error'] = $exception->getMessage();
        }
        return new RedirectResponse(
            $this->socialLoginPrefixUrl . 'email/code?state=' . rawurlencode(SocialLogin::generateState($state))
        );
    }
}
