<?php

namespace Miniframe\SocialLogin\Controller;

use Miniframe\Core\AbstractController;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Response\PhpResponse;
use Miniframe\Response\RedirectResponse;
use Miniframe\SocialLogin\Middleware\SocialLogin;
use Miniframe\SocialLogin\Provider\ProviderInterface;
use Miniframe\SocialLogin\Provider\Email;

class Login extends AbstractSocialLoginController
{
    /**
     * Reference to a provider
     *
     * @var ProviderInterface|null
     */
    protected $provider;

    /**
     * List of known providers.
     *
     * @var array
     */
    protected $providers;

    /**
     * Initializes the Login Controller
     *
     * @param Request                $request              Reference to the Request object.
     * @param Config                 $config               Reference to the Config object.
     * @param SocialLogin            $socialLogin          Reference to the SocialLogin middleware.
     * @param string                 $socialLoginPrefixUrl Base URL for the Social Login pages.
     * @param array                  $providers            List of known providers.
     * @param ProviderInterface|null $provider             Provider class, if known.
     */
    public function __construct(
        Request $request,
        Config $config,
        SocialLogin $socialLogin,
        string $socialLoginPrefixUrl,
        array $providers,
        ?ProviderInterface $provider = null
    ) {
        parent::__construct($request, $config, $socialLogin, $socialLoginPrefixUrl);
        $this->providers = $providers;
        $this->provider = $provider;
    }

    /**
     * Returns the login page
     *
     * @return Response
     */
    public function login(): Response
    {
        $state = SocialLogin::parseState($this->request->getRequest('state'));

        // When a provider is defined, log in with that provider
        if ($this->provider) {
            $this->socialLogin->login($this->provider->authenticate());
            return new RedirectResponse($state['redirectUrl'] ?? $this->config->get('framework', 'base_href'));
        }

        // Is there just one provider that's not Email, and autologin is enabled? Redirect directly
        if (
            count($this->providers) === 1
            && !isset($this->providers['email'])
            && $this->config->has('sociallogin', 'autologin')
            && $this->config->get('sociallogin', 'autologin') == true
        ) {
            $providerName = array_keys($this->providers)[0];
            $redirectUrl = $this->socialLoginPrefixUrl
                . 'login/' . $providerName
                . '?state=' . rawurlencode($this->request->getRequest('state'))
            ;
            return new RedirectResponse($redirectUrl);
        }

        // Get provider themes
        $providerImages = array();
        $providerColors = array();
        foreach ($this->providers as $provider => $class) {
            $providerImages[$provider] = $class::getLogoSource();
            $providerColors[$provider] = $class::getThemeColor();
        }

        // Define the public href for the "Close" button
        $publicHref = null;
        if ($this->config->has('sociallogin', 'public_href')) {
            $publicHref = $this->config->get('sociallogin', 'public_href');
        }

        // Do we have an error?
        $error = $state['error'] ?? null;
        $state['error'] = null;

        return new PhpResponse(__DIR__ . '/../../templates/login.html.php', [
            'socialLoginPrefixUrl' => $this->socialLoginPrefixUrl,
            'providers' => array_keys(array_diff($this->providers, [Email::class])),
            'providerImages' => $providerImages,
            'providerColors' => $providerColors,
            'publicHref' => $publicHref,
            'emailEnabled' => in_array(Email::class, $this->providers),
            'state' => SocialLogin::generateState($state),
            'error' => $error,
            'darkmode' => $this->darkmode,
        ]);
    }

    /**
     * Logs out the current user and redirects to the previous page.
     *
     * @return Response
     */
    public function logout(): Response
    {
        $this->socialLogin->logout();

        $state = SocialLogin::parseState($this->request->getRequest('state'));
        return new RedirectResponse($state['redirectUrl'] ?? $this->config->get('framework', 'base_href'));
    }
}
