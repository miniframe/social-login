<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Middleware\Session;
use Miniframe\SocialLogin\Model\User;

interface ProviderInterface
{
    /**
     * Creates a new provider
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config);

    /**
     * Starts the authentication process
     *
     * @return User
     */
    public function authenticate(): User;

    /**
     * Returns the image source for the logo of this provider.
     *
     * @return string
     */
    public static function getLogoSource(): string;

    /**
     * Returns the theme color for this provider.
     *
     * @return string
     */
    public static function getThemeColor(): string;
}
