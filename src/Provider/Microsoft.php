<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\SocialLogin\Model\User;

class Microsoft extends AbstractOAuth2Provider
{
    /**
     * Returns the Authorize URL
     *
     * @return string
     */
    protected function getAuthorizeUrl(): string
    {
        return 'https://login.microsoftonline.com/' . $this->tenant . '/oauth2/v2.0/authorize';
    }

    /**
     * Returns the Access Token URL
     *
     * @return string
     */
    protected function getAccessTokenUrl(): string
    {
        return 'https://login.microsoftonline.com/' . $this->tenant . '/oauth2/v2.0/token';
    }

    /**
     * The tenant ID. Valid values are common, organizations, consumers, and tenant identifiers.
     *
     * @var string
     *
     * @see https://learn.microsoft.com/en-us/entra/identity-platform/v2-oauth2-auth-code-flow
     */
    protected $tenant = 'common';

    /**
     * Returns the requested scope
     *
     * @return string|null
     */
    protected function getScope(): ?string
    {
        return 'User.Read';
    }

    /**
     * Creates a new Microsoft OAuth 2.0 provider
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);
        if ($config->has('sociallogin-microsoft', 'tenant')) {
            $this->tenant = $config->get('sociallogin-microsoft', 'tenant');
        }
    }

    /**
     * Returns the user profile
     *
     * @param array $accessToken The access token.
     *
     * @return User
     */
    protected function getUserProfile(array $accessToken): User
    {
        $userData = $this->curlRequest('https://graph.microsoft.com/v1.0/me', 'GET', [], [
            'Authorization: Bearer ' . $accessToken['access_token'],
        ]);

        // When fetching the avatar fails, fall back to Gravatar
        $avatar = 'https://s.gravatar.com/avatar/' . md5(strtolower(trim($userData['userPrincipalName'])))
            . '?s=80&d=identicon';

        foreach (
            [
                // For business accounts (Office 365), the avatar can be located here:
                'https://graph.microsoft.com/v1.0/me/photo/$value',
                // For personal accounts (Hotmail, etc.), the avatar can be located here:
                'https://graph.microsoft.com/beta/me/photo/$value'
            ] as $url
        ) {
            try {
                $avatarBlob = $this->curlRequest($url, 'GET', [], [
                    'Authorization: Bearer ' . $accessToken['access_token'],
                ], true);
                $avatar = 'data:' . $avatarBlob['contentType'] . ';base64,'
                    . base64_encode($avatarBlob['responseBody']);
                break;
            } catch (\RuntimeException $exception) {
                if ($exception->getMessage() == 'HTTP #404 error') {
                    continue;
                }
                throw $exception;
            }
        }

        return new User(
            $userData['id'],
            $userData['userPrincipalName'],
            $userData['displayName'],
            $avatar,
            static::class,
            ['accessToken' => $accessToken, 'userData' => $userData]
        );
    }

    /**
     * Returns the image source for the logo of this provider.
     *
     * @return string
     */
    public static function getLogoSource(): string
    {
        return 'data:image/svg+xml;base64,'
            . base64_encode(file_get_contents(__DIR__ . '/../../templates/logos/Microsoft.svg'));
    }

    /**
     * Returns the theme color for this provider.
     *
     * @return string
     */
    public static function getThemeColor(): string
    {
        return 'rgb(0, 103, 184)';
    }
}
