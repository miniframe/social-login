# Gitlab OAuth 2.0 provider

This provider is written to support Gitlab logins. For Gitlabs documentation, see
https://docs.gitlab.com/ee/api/oauth2.html

## How to configure this provider

For OAuth 2.0 providers, you'll need a client ID and client secret.
To get them, you need to register an app at the login provider.

For Gitlab, you can do that by following these steps:

* In the upper-right corner of any page, click your profile photo, then click `Preferences`.
* In the left sidebar click `Applications` and populate the following fields:  
  **Name:** The name of your app  
  **Redirect URI:** Something like `https://{your-domain-here}/_SOCIALLOGIN/login/gitlab`
  **Scopes:** Check `read_user`
* Now click `Save application`
* Here you'll see the `Application ID` that can be used as `client_id`
* Click on `Copy` next to the `Secret` to get the `client_secret` in your clipboard.
* Put this in your `sociallogin.ini` file:
  ```ini
  [sociallogin]
  providers[] = Gitlab

  [sociallogin-gitlab]
  client_id = ****************************************************************
  ```  
  And this in your `secrets.ini` file:
  ```ini
  [sociallogin-gitlab]
  client_secret = ****************************************************************
  ```  
  It's recommended to add the secrets.ini file in your `.gitignore` file, so secrets will never end up in your version control software.
