<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\SocialLogin\Model\User;

class Facebook extends AbstractOAuth2Provider
{
    /**
     * Returns the Authorize URL
     *
     * @return string
     */
    protected function getAuthorizeUrl(): string
    {
        return 'https://www.facebook.com/v11.0/dialog/oauth';
    }

    /**
     * Returns the Access Token URL
     *
     * @return string
     */
    protected function getAccessTokenUrl(): string
    {
        return 'https://graph.facebook.com/v11.0/oauth/access_token';
    }

    /**
     * Returns the requested scope
     *
     * @return string|null
     */
    protected function getScope(): ?string
    {
        return null;
    }

    /**
     * Returns the user profile
     *
     * @param array $accessToken The access token.
     *
     * @return User
     */
    protected function getUserProfile(array $accessToken): User
    {
        // Fetch user meta data
        $userData = $this->curlRequest('https://graph.facebook.com/me', 'GET', null, [
            'Authorization: Bearer ' . $accessToken['access_token'],
        ]);

        // Fetch avatar meta data
        $avatarData = $this->curlRequest(
            'https://graph.facebook.com/v11.0/me/picture?format=json&redirect=false&height=128&width=128',
            'GET',
            null,
            [
                'Authorization: Bearer ' . $accessToken['access_token'],
                'Accept: application/json'
            ]
        );

        // Fetch avatar binary; this is required because of QORS on the image URL at Facebook
        $avatarBlob = $this->curlRequest($avatarData['data']['url'], 'GET', [], [
            'Authorization: Bearer ' . $accessToken['access_token'],
        ], true);
        $avatarSource = 'data:' . $avatarBlob['contentType'] . ';base64,' . base64_encode($avatarBlob['responseBody']);

        return new User(
            $userData['id'],
            $userData['id'],
            $userData['name'],
            $avatarSource,
            static::class,
            ['accessToken' => $accessToken, 'userData' => $userData, 'avatarData' => $avatarData]
        );
    }

    /**
     * Returns the image source for the logo of this provider.
     *
     * @return string
     */
    public static function getLogoSource(): string
    {
        return 'data:image/svg+xml;base64,'
            . base64_encode(file_get_contents(__DIR__ . '/../../templates/logos/Facebook.svg'));
    }

    /**
     * Returns the theme color for this provider.
     *
     * @return string
     */
    public static function getThemeColor(): string
    {
        return 'rgba(49,135,255,255)';
    }
}
