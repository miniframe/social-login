<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\SocialLogin\Model\User;

class Twitter extends AbstractOAuth1Provider
{
    /**
     * Returns the request token URL
     *
     * @return string
     */
    protected function getRequestTokenUrl(): string
    {
        return 'https://api.twitter.com/oauth/request_token';
    }

    /**
     * Returns the authorize URL
     *
     * @return string
     */
    protected function getAuthorizeUrl(): string
    {
        return 'https://api.twitter.com/oauth/authorize';
    }

    /**
     * Returns the access token URL
     *
     * @return string
     */
    protected function getAccessTokenUrl(): string
    {
        return 'https://api.twitter.com/oauth/access_token';
    }

    /**
     * Returns the user profile
     *
     * @param array $accessToken The access token.
     *
     * @return User
     */
    protected function getUserProfile(array $accessToken): User
    {
        // Compiles the request
        $data = [
            'screen_name' => $accessToken['screen_name'],
            'oauth_token' => $accessToken['oauth_token'],
        ];
        $requestMethod = 'GET';
        $url = 'https://api.twitter.com/1.1/users/show.json';
        $headers = [$this->generateOAuthHeader($url, 'GET', $data, $accessToken['oauth_token_secret'])];

        // Fetch user data
        $data = $this->curlRequest($url, $requestMethod, $data, $headers);

        return new User(
            $accessToken['user_id'],
            $accessToken['screen_name'],
            $data['name'],
            $data['profile_image_url_https'],
            __CLASS__,
            ['accessToken' => $accessToken, 'userData' => $data]
        );
    }

    /**
     * Returns the image source for the logo of this provider.
     *
     * @return string
     */
    public static function getLogoSource(): string
    {
        return 'data:image/svg+xml;base64,'
            . base64_encode(file_get_contents(__DIR__ . '/../../templates/logos/Twitter.svg'));
    }

    /**
     * Returns the theme color for this provider.
     *
     * @return string
     */
    public static function getThemeColor(): string
    {
        return 'rgba(30,41,51,255)';
    }
}
