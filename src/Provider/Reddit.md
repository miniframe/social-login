# Reddit OAuth 2.0 provider

This provider is written to support Reddit logins. For Reddits documentation, see
https://github.com/reddit-archive/reddit/wiki/OAuth2

## How to configure this provider

For OAuth 2.0 providers, you'll need a client ID and client secret.
To get them, you need to register an app at the login provider.

For Reddit, you can do that by following these steps:

* Go to https://www.reddit.com/prefs/apps
* Click `are you a developer? create an ap...`
* Sekect `web app`and give the app a name
* For `redirect uri` fill in something like: `https://{your-domain-here}/_SOCIALLOGIN/login/reddit`
* Underneath `web app` you'll see your client_id and your secret is also visible now. Put them in your `sociallogin.ini` file:  
  ```ini
  [sociallogin]
  providers[] = Reddit
  
  [sociallogin-reddit]
  client_id = ************-*********
  ```
  And in your `secrets.ini`:  
  ```ini
  [sociallogin-reddit]
  client_secret = ******************************
  ```
  It's recommended to add the secrets.ini file in your `.gitignore` file, so secrets will never end up in your version control software.
