<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\Core\Config;
use Miniframe\Core\Request;

abstract class AbstractOAuthProvider implements ProviderInterface
{
    /**
     * Short name of the provider (Example: 'twitter')
     *
     * @var string
     */
    protected $shortName;
    /**
     * Reference to the Request object
     *
     * @var Request
     */
    protected $request;
    /**
     * Base href for this application
     *
     * @var string
     */
    protected $baseHref;
    /**
     * User agent for the cURL requests
     *
     * @var string
     */
    protected $userAgent = 'Miniframe Social Login client (https://bitbucket.org/miniframe/miniframe-social-login/)';

    /**
     * Creates a new OAuth provider
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        $this->request = $request;
        $this->shortName = strtolower(array_reverse(explode('\\', get_called_class()))[0]);
        $this->baseHref = $config->get('framework', 'base_href');
    }

    /**
     * Returns the current URI
     *
     * @return string
     */
    protected function getCurrentUri(): string
    {
        $parts = explode(':', $this->request->getServer('HTTP_HOST'), 2);
        if (count($parts) == 2) {
            $serverHost = $parts[0];
            $serverPort = $parts[1];
        } else {
            $serverHost = $this->request->getServer('HTTP_HOST');
            $serverPort = (int)$this->request->getServer('SERVER_PORT');
        }
        $requestPath = explode('?', $this->request->getServer('REQUEST_URI'))[0];

        if ($this->request->getServer('HTTPS')) {
            $serverProtocol = 'https://';
            $defaultPort = 443;
        } else {
            $serverProtocol = 'http://';
            $defaultPort = 80;
        }
        return $serverProtocol . $serverHost . ($defaultPort == $serverPort ? '' : ':' . $serverPort) . $requestPath;
    }


    /**
     * Performs a cURL request
     *
     * @param string     $url           The URL.
     * @param string     $requestMethod The request method (GET, POST).
     * @param array|null $data          Request data.
     * @param array      $headers       Request headers.
     * @param boolean    $raw           Set to true to get the raw response.
     *
     * @return array
     */
    protected function curlRequest(
        string $url,
        string $requestMethod,
        array $data = null,
        array $headers = array(),
        bool $raw = false
    ): array {
        // Default options
        $curlOptions = array();
        $curlOptions[CURLOPT_URL] = $url;
        $curlOptions[CURLOPT_RETURNTRANSFER] = true;
        $curlOptions[CURLOPT_HEADER] = true;
        $curlOptions[CURLOPT_HTTPHEADER] = $headers;
        $curlOptions[CURLOPT_USERAGENT] = $this->userAgent;

        if (strtoupper($requestMethod) == 'GET') {
            $curlOptions[CURLOPT_HTTPGET] = true;
            if ($data) {
                $curlOptions[CURLOPT_URL] .= (strpos('?', $curlOptions[CURLOPT_URL]) !== false ? '&' : '?')
                    . http_build_query($data);
            }
        } elseif (strtoupper($requestMethod) == 'POST') {
            // When doing a regular POST request, CURLOPT_POSTFIELDS is provided as string, the content-type will be
            // 'application/x-www-form-urlencoded'.
            $curlOptions[CURLOPT_POSTFIELDS] = http_build_query($data);
            $curlOptions[CURLOPT_POST] = true;
        } else {
            throw new \RuntimeException('Can\'t handle request method "' . strtoupper($requestMethod) . '"');
        }

        // Executes the request
        $ch = curl_init();
        curl_setopt_array($ch, $curlOptions);
        $data = curl_exec($ch);

        // When something went wrong, throw an exception
        if ($data === false) {
            throw new \RuntimeException('Unexpected cURL error #' . curl_errno($ch) . ': ' . curl_error($ch));
        }

        // Fetch info and close handle
        $info = curl_getinfo($ch);
        curl_close($ch);

        // We'll expect all OK variants, but nothing else.
        if ($info['http_code'] < 200 || $info['http_code'] > 299) {
            throw new \RuntimeException('HTTP #' . $info['http_code'] . ' error');
        }

        // Splits data
        $responseHeaders = substr($data, 0, $info['header_size']);
        $responseBody = substr($data, $info['header_size']);
        $contentType = $info['content_type'] ?? 'application/json';
        if (strpos($contentType, ';') !== false) {
            $contentType = explode(';', $contentType)[0];
        }

        // Raw response
        if ($raw) {
            return array(
                'responseHeaders' => $responseHeaders,
                'responseBody' => $responseBody,
                'contentType' => $contentType,
                'connectionInfo' => $info,
            );
        }

        // Parsed response
        switch ($contentType) {
            case 'application/json':
                $responseData = json_decode($responseBody, true, 512, JSON_THROW_ON_ERROR);
                break;
            case 'application/x-www-form-urlencoded':
                parse_str($responseBody, $responseData);
                break;
            default:
                // Try to autodetect; some providers (Twitter!) don't use the correct Content-type header.
                if (preg_match('/^[a-z=0-9_\-&%]+$/i', $responseBody) && strpos($responseBody, '=') !== false) {
                    parse_str($responseBody, $responseData);
                } else {
                    throw new \RuntimeException('Could not decode the ' . $contentType . ' response');
                }
        }

        return $responseData;
    }
}
