# Facebook OAuth 2.0 provider

This provider is written to support Facebook logins. For Facebooks documentation, see
https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow

## How to configure this provider

For OAuth 2.0 providers, you'll need a client ID and client secret.
To get them, you need to register an app at the login provider.

For Facebook, you can do that by following these steps:

* Go to https://developers.facebook.com/apps/ and click `Create App`
* Select `None` and click `Continue`
* Give the app a name and click `Create App`
* Click `Set Up` at `Facebook Login`
* Select `Web`, fill in the website and click `Save`
* Now click `Settings` in the bar at the left, underneath `Facebook Login`
* Go to `Valid OAuth Redirect URIs` and add an URL like `https://{your-domain-here}/_SOCIALLOGIN/login/facebook` and click `Save changes`
* Now, at the left top, click on `Settings` and then `Basic`
* Here you'll find the `App ID` (client_id) and `App Secret` (client_secret)
* Put them in your `sociallogin.ini` file:
  ```ini
  [sociallogin]
  providers[] = Facebook

  [sociallogin-facebook]
  client_id = ****************
  ```  
  And this in your `secrets.ini` file:
  ```ini
  [sociallogin-facebook]
  client_secret = ********************************
  ```  
  It's recommended to add the secrets.ini file in your `.gitignore` file, so secrets will never end up in your version control software.
