# Atlassian OAuth 2.0 provider

This provider is written to support Atlassian logins. For Atlassians documentation, see
https://developer.atlassian.com/cloud/jira/platform/oauth-2-3lo-apps/

**⚠️This Provider only works for the Atlassian Cloud domain in which you configure the App, for regular Atlassian logins, use the [Bitbucket](Bitbucket.md) provider.**

## How to configure this provider

For OAuth 2.0 providers, you'll need a client ID and client secret.
To get them, you need to register an app at the login provider.

For Atlassian, you can do that by following these steps:

* Go to the Atlassian Developer Console at https://developer.atlassian.com/console/myapps/
* Click on `Create` and then on `OAuth 2.0 integration`
* Give your app a name, read their terms, check the box and click `Create`
* Open the app overview, and go to `Distribution`. Populate this as good as you can.
* Go to `Authorization` in the app overview and click on `Configure` next to `OAuth 2.0`
* Populate the `Callback URL` with `https://{your-domain-here}/_SOCIALLOGIN/login/atlassian`
* Go to `Permissions` in the app overview and click `Add` next to `User identity API`
* Next, go to `Settings` in the app overview. Here you'll find a `Client ID` and `Secret`
* Put this in your `sociallogin.ini` file:  
  ```ini
  [sociallogin]
  providers[] = Atlassian

  [sociallogin-atlassian]
  client_id = ********************************
  ```  
  And this in your `secrets.ini` file:  
  ```ini
  [sociallogin-atlassian]
  client_secret = ****************************************************************
  ```  
  It's recommended to add the secrets.ini file in your `.gitignore` file, so secrets will never end up in your version control software.
