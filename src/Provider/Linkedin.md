# Linkedin OAuth 2.0 provider

This provider is written to support Linkedin logins. For Linkedins documentation, see
https://docs.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow?context=linkedin%2Fcontext&tabs=HTTPS

## How to configure this provider

For OAuth 2.0 providers, you'll need a client ID and client secret.
To get them, you need to register an app at the login provider.

For Linkedin, you can do that by following these steps:

* Open https://developer.linkedin.com/ and click `My apps` in the top right corner.
* Click `Create app`. Fill in the following form as good as possible and click `Create app` again.
* Open the tab `Auth` in the top. Here you'll find the client ID and client secret. Put them in your `sociallogin.ini` file:  
  ```ini
  [sociallogin]
  providers[] = Linkedin
  
  [sociallogin-linkedin]
  client_id = **************
  ```
  And in your `secrets.ini`:  
  ```ini
  [sociallogin-linkedin]
  client_secret = ****************
  ```
  It's recommended to add the secrets.ini file in your `.gitignore` file, so secrets will never end up in your version control software.
* Click on the pen icon next to `Authorized redirect URLs for your app` and click `Add redirect URL`
* Populate the field with something like `https://{your-domain-here}/_SOCIALLOGIN/login/linkedin` and click `Update`
* Open the tab `Products` in the top. Here you can click `Select` for `Sign In with LinkedIn`.
