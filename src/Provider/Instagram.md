# Instagram OAuth 2.0 provider

This provider is written to support Instagram logins. For Instagrams documentation, see
https://developers.facebook.com/docs/instagram-basic-display-api

## How to configure this provider

For OAuth 2.0 providers, you'll need a client ID and client secret.
To get them, you need to register an app at the login provider.

For Instagram, you should follow the following steps:

* Go to https://developers.facebook.com/apps/ and click `Create App`
* Select `None` and click `Continue`
* Give the app a name and click `Create App`
* Click on `Set Up` at `Instagram Basic Display`
* Click `Create New App` on the bottom, provide a name and click `Create App`.
* You now see an `Instagram App ID` (client_id) and `Instagram App Secret` (client_secret)
* Put these secrets in your `sociallogin.ini` file for Instagram:
  ```ini
  [sociallogin]
  providers[] = Instagram

  [sociallogin-instagram]
  client_id = ***************
  ```  
  And this in your `secrets.ini` file:
  ```ini
  [sociallogin-instagram]
  client_secret = ********************************
  ```  
  It's recommended to add the secrets.ini file in your `.gitignore` file, so secrets will never end up in your version control software.
* Scroll down to `Valid OAuth Redirect URIs` and fill in `https://{your-domain-here}/_SOCIALLOGIN/login/instagram`
* Scroll further down and click `Add to submission` after `instagram_graph_user_profile`
* Click `Edit details` and populate the popup.
