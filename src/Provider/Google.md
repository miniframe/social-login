# Google OAuth 2.0 provider

This provider is written to support Google logins. For Googles documentation, see
https://developers.google.com/identity/protocols/oauth2

## How to configure this provider

For OAuth 2.0 providers, you'll need a client ID and client secret.
To get them, you need to register an app at the login provider.

For Google, you can do that by following these steps:

* Open https://console.developers.google.com/
* Click `CREATE PROJECT` on the top right
* Give your project a name and click `CREATE`
* Click on `OAuth consent screen` on the left and fill this in as good as possible
* Click `ADD OR REMOVE SCOPES` and add:
  * `.../auth/userinfo.email`
  * `.../auth/userinfo.profile`
  * `openid`
* Click `SAVE AND CONTINUE`
* Click `Credentials` in the left bar
* Click `+ CREATE CREDENTIALS` -> `OAuth Client ID`
* Select `Web application` and give this a name
* Click `ADD URI` at `Authorized redirect URIs` and fill in something like `https://{your-domain-here}/_SOCIALLOGIN/login/google`
* Click `CREATE` and you'll get a Client ID and Client Secret
* Put this in your `sociallogin.ini` file:
  ```ini
  [sociallogin]
  providers[] = Google

  [sociallogin-google]
  client_id = ************-********************************.apps.googleusercontent.com
  ```  
  And this in your `secrets.ini` file:
  ```ini
  [sociallogin-google]
  client_secret = "************************"
  ```  
  It's recommended to add the secrets.ini file in your `.gitignore` file, so secrets will never end up in your version control software.
