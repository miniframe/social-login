<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\SocialLogin\Model\User;

class Atlassian extends AbstractOAuth2Provider
{
    /**
     * Returns the Authorize URL
     *
     * @return string
     */
    protected function getAuthorizeUrl(): string
    {
        return 'https://auth.atlassian.com/authorize';
    }

    /**
     * Returns the Access Token URL
     *
     * @return string
     */
    protected function getAccessTokenUrl(): string
    {
        return 'https://auth.atlassian.com/oauth/token';
    }

    /**
     * Returns the requested scope
     *
     * @return string|null
     */
    protected function getScope(): ?string
    {
         return 'read:me';
    }

    /**
     * Gets a full authorize URL, including the client ID and all other required parameters.
     *
     * @param array $data Optionally extra fields to append to the authorize URL.
     *
     * @return string
     */
    protected function getFullAuthorizeUrl(array $data = array()): string
    {
        // Atlassian requires these fields also to be defined
        $data['audience'] = 'api.atlassian.com';
        $data['prompt'] = 'consent';
        return parent::getFullAuthorizeUrl($data);
    }

    /**
     * Returns the user profile
     *
     * @param array $accessToken The access token.
     *
     * @return User
     */
    protected function getUserProfile(array $accessToken): User
    {
        $data = $this->curlRequest('https://api.atlassian.com/me', 'GET', null, [
            'Authorization: Bearer ' . $accessToken['access_token'],
            'Accept: application/json',
        ]);
        if (!$data['email_verified']) {
            throw new \RuntimeException('The email address for this Atlassian account is not yet verified');
        }

        return new User(
            $data['account_id'],
            $data['email'],
            $data['name'],
            $data['picture'],
            static::class,
            ['accessToken' => $accessToken, 'userData' => $data]
        );
    }

    /**
     * Returns the image source for the logo of this provider.
     *
     * @return string
     */
    public static function getLogoSource(): string
    {
        return 'data:image/svg+xml;base64,'
            . base64_encode(file_get_contents(__DIR__ . '/../../templates/logos/Atlassian.svg'));
    }

    /**
     * Returns the theme color for this provider.
     *
     * @return string
     */
    public static function getThemeColor(): string
    {
        return 'rgba(16,76,180,255)';
    }
}
