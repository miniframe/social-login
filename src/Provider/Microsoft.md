# Microsoft OAuth 2.0 provider

This provider is written to support Microsoft logins. For Microsofts documentation, see
https://docs.microsoft.com/en-us/graph/auth-v2-user

## How to configure this provider

For OAuth 2.0 providers, you'll need a client ID and client secret.
To get them, you need to register an app at the login provider.

For Microsoft, you can do that by following these steps:

* Open the [Microsoft Azure App Portal](https://go.microsoft.com/fwlink/?linkid=2083908).
* Click on `Register an application`
* Fill in a name, check `Personal accounts only`
* Populate the `Redirect URI` with something like `https://{your-domain-here}/_SOCIALLOGIN/login/microsoft`
* Take the `Application (client) ID` from the `Overview` and use it as `client_id` in your .ini file:  
  ```ini
  [sociallogin]
  providers[] = Microsoft
  
  [sociallogin-microsoft]
  client_id = ********-****-****-****-************
  ; Tenant; valid values are common, organizations, consumers, and tenant identifiers.
  tenant = common
  ```
* Click on `Certificates & secrets` at the left and click `New client secret`
* Enter a description and an expiration time. Regretfully, it's not possible to make them last indefinitely.
* Copy the `Value` and put it in your `secrets.ini`:  
  ```ini
  [sociallogin-microsoft]
  client_secret = "**********************************"
  ```
  It's recommended to add the secrets.ini file in your `.gitignore` file, so secrets will never end up in your version control software.
