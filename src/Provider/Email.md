# Email provider

This provider is written to support account-less email login.
When the user provides its email address, a one-time code will be sent to the address to validate the email address.

## How to configure this provider

To configure this provider, add the values below to your configuration file.

```ini
[sociallogin]
providers[] = Email

[sociallogin-email]
from_name = Miniframe Social Login
from_email = miniframe@example.com
```
