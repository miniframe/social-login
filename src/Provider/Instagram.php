<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\SocialLogin\Model\User;

class Instagram extends AbstractOAuth2Provider
{
    /**
     * Returns the Authorize URL
     *
     * @return string
     */
    protected function getAuthorizeUrl(): string
    {
        return 'https://api.instagram.com/oauth/authorize';
    }

    /**
     * Returns the Access Token URL
     *
     * @return string
     */
    protected function getAccessTokenUrl(): string
    {
        return 'https://api.instagram.com/oauth/access_token';
    }

    /**
     * Returns the requested scope
     *
     * @return string|null
     */
    protected function getScope(): ?string
    {
        return 'user_profile user_media';
    }

    /**
     * Returns the user profile
     *
     * @param array $accessToken The access token.
     *
     * @return User
     */
    protected function getUserProfile(array $accessToken): User
    {
        // Fetch username and ID
        $userData = $this->curlRequest('https://graph.instagram.com/v11.0/me?fields=id,username', 'GET', null, [
            'Authorization: Bearer ' . $accessToken['access_token'],
        ]);

        try {
            $this->userAgent = $this->request->getServer('HTTP_USER_AGENT');
            $extendedData = $this->curlRequest(
                'https://www.instagram.com/' . $userData['username'] . '/?__a=1',
                'GET',
                [],
                [
                    'X-Forwarded-For: ' . $this->request->getServer('REMOTE_ADDR'),
                    'Accept: ' . $this->request->getServer('HTTP_ACCEPT'),
                    'Accept-Language: ' . $this->request->getServer('HTTP_ACCEPT_LANGUAGE'),
                    'Connection: Close',
                    'Cache-Control: max-age=0',
                ]
            );

            $profilePicture = $extendedData['graphql']['user']['profile_pic_url'];
            $fullName = $extendedData['graphql']['user']['full_name'];
        } catch (\RuntimeException $exception) {
            // Redirected to the login page; rate limit for anonymous users exceeded
            if ($exception->getMessage() != 'HTTP #302 error') {
                throw $exception;
            }
            $extendedData = null;
        }

        if (isset($profilePicture)) {
            // Fetch profile picture, because we can't embed it on the site because of the Facebook Container in Firefox
            $profileBlob = $this->curlRequest($profilePicture, 'GET', [], [
                'X-Forwarded-For: ' . $this->request->getServer('REMOTE_ADDR'),
                'Connection: Close',
                'Cache-Control: max-age=0',
            ], true);
            $profilePicture = 'data:' . $profileBlob['contentType']
                . ';base64,' . base64_encode($profileBlob['responseBody']);
        } else {
            // When no profile picture can be found, use a default one
            $profilePicture = 'data:image/svg+xml;base64,'
                . base64_encode(file_get_contents(__DIR__ . '/../../templates/unknown-user.svg'));
        }

        return new User(
            $userData['id'],
            $userData['username'],
            $fullName ?? $userData['username'],
            $profilePicture,
            static::class,
            ['accessToken' => $accessToken, 'userData' => $userData, 'extendedData' => $extendedData]
        );
    }

    /**
     * Returns the image source for the logo of this provider.
     *
     * @return string
     */
    public static function getLogoSource(): string
    {
        return 'data:image/svg+xml;base64,'
            . base64_encode(file_get_contents(__DIR__ . '/../../templates/logos/Instagram.svg'));
    }

    /**
     * Returns the theme color for this provider.
     *
     * @return string
     */
    public static function getThemeColor(): string
    {
        return 'rgb(193,53,132)';
    }

    /**
     * Instagram really wants HTTPS, even when developing.
     *
     * @return string
     */
    protected function getCurrentUri(): string
    {
        $url = parent::getCurrentUri();
        if (substr($url, 0, 5) == 'http:') {
            $url = 'https:' . substr($url, 5);
        }
        return $url;
    }
}
