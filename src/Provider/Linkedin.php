<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\SocialLogin\Model\User;

class Linkedin extends AbstractOAuth2Provider
{
    /**
     * Returns the Authorize URL
     *
     * @return string
     */
    protected function getAuthorizeUrl(): string
    {
        return 'https://www.linkedin.com/oauth/v2/authorization';
    }

    /**
     * Returns the Access Token URL
     *
     * @return string
     */
    protected function getAccessTokenUrl(): string
    {
        return 'https://www.linkedin.com/oauth/v2/accessToken';
    }

    /**
     * Returns the requested scope
     *
     * @return string|null
     */
    protected function getScope(): ?string
    {
        return 'r_liteprofile';
    }

    /**
     * Returns the user profile
     *
     * @param array $accessToken The access token.
     *
     * @return User
     */
    protected function getUserProfile(array $accessToken): User
    {
        $data = $this->curlRequest(
            'https://api.linkedin.com/v2/me?projection=(id,localizedFirstName,localizedLastName,'
            . 'profilePicture(displayImage~:playableStreams))',
            'GET',
            null,
            [
                'Authorization: Bearer ' . $accessToken['access_token'],
            ]
        );

        // Tries to find a profile picture
        $profilePicture = null;
        if (
            isset($data['profilePicture'])
            && isset($data['profilePicture']['displayImage~'])
            && isset($data['profilePicture']['displayImage~']['elements'])
        ) {
            foreach ($data['profilePicture']['displayImage~']['elements'] as $profilePictureElement) {
                if (
                    isset($profilePictureElement['identifiers'][0]['identifier'])
                    && $profilePictureElement['identifiers'][0]['identifierType'] == 'EXTERNAL_URL'
                ) {
                    $profilePicture = $profilePictureElement['identifiers'][0]['identifier'];
                    break;
                }
            }
        }

        // When no profile picture can be found, use a default one
        if ($profilePicture === null) {
            $profilePicture = 'data:image/svg+xml;base64,'
                . base64_encode(file_get_contents(__DIR__ . '/../../templates/unknown-user.svg'));
        }

        return new User(
            $data['id'],
            $data['id'],
            $data['localizedFirstName'] . ' ' . $data['localizedLastName'],
            $profilePicture,
            static::class,
            ['accessToken' => $accessToken, 'userData' => $data]
        );
    }

    /**
     * Returns the image source for the logo of this provider.
     *
     * @return string
     */
    public static function getLogoSource(): string
    {
        return 'data:image/svg+xml;base64,'
            . base64_encode(file_get_contents(__DIR__ . '/../../templates/logos/Linkedin.svg'));
    }

    /**
     * Returns the theme color for this provider.
     *
     * @return string
     */
    public static function getThemeColor(): string
    {
        return 'rgb(40,103,178)';
    }
}
