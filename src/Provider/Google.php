<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\SocialLogin\Model\User;

class Google extends AbstractOAuth2Provider
{
    /**
     * Returns the Authorize URL
     *
     * @return string
     */
    protected function getAuthorizeUrl(): string
    {
        return 'https://accounts.google.com/o/oauth2/v2/auth';
    }

    /**
     * Returns the Access Token URL
     *
     * @return string
     */
    protected function getAccessTokenUrl(): string
    {
        return 'https://oauth2.googleapis.com/token';
    }

    /**
     * Returns the requested scope
     *
     * @return string|null
     */
    protected function getScope(): ?string
    {
        return
            'https://www.googleapis.com/auth/userinfo.email'
            . ' https://www.googleapis.com/auth/userinfo.profile'
            . ' openid'
        ;
    }

    /**
     * Returns the user profile
     *
     * @param array $accessToken The access token.
     *
     * @return User
     */
    protected function getUserProfile(array $accessToken): User
    {
        $data = $this->curlRequest('https://www.googleapis.com/oauth2/v1/userinfo?alt=json', 'GET', null, [
            'Authorization: Bearer ' . $accessToken['access_token'],
        ]);

        if (isset($data['verified_email']) && $data['verified_email'] === false) {
            throw new \RuntimeException('Email address not verified');
        }

        return new User(
            $data['id'],
            $data['email'],
            $data['name'],
            $data['picture'],
            static::class,
            ['accessToken' => $accessToken, 'userData' => $data]
        );
    }

    /**
     * Returns the image source for the logo of this provider.
     *
     * @return string
     */
    public static function getLogoSource(): string
    {
        return 'data:image/svg+xml;base64,'
            . base64_encode(file_get_contents(__DIR__ . '/../../templates/logos/Google.svg'));
    }

    /**
     * Returns the theme color for this provider.
     *
     * @return string
     */
    public static function getThemeColor(): string
    {
        return '#d3a300';
    }
}
