# Bitbucket OAuth 2.0 provider

This provider is written to support Bitbucket logins. For Bitbuckets documentation, see
https://developer.atlassian.com/cloud/bitbucket/oauth-2/

## How to configure this provider

For OAuth 2.0 providers, you'll need a client ID and client secret.
To get them, you need to register an app at the login provider.

For Bitbucket, you can do that by following these steps:

* Go to the Bitbucket Workspaces page at https://bitbucket.org/account/workspaces/
* Select your workspace, click on `Settings` and then on `OAuth Consumers` at the left.
* Click on `Add consumer`.
* Give your app a name and populate the `Callback URL` with `https://{your-domain-here}/_SOCIALLOGIN/login/bitbucket`
* Select `Read` under `Account` `Permissions` and click `Save`
* Now click on your newly created app and you'll a `key` (client_id) and `secret` (client_secret)
* Put this in your `sociallogin.ini` file:
  ```ini
  [sociallogin]
  providers[] = Bitbucket

  [sociallogin-bitbucket]
  client_id = ******************
  ```  
  And this in your `secrets.ini` file:
  ```ini
  [sociallogin-bitbucket]
  client_secret = ********************************
  ```  
  It's recommended to add the secrets.ini file in your `.gitignore` file, so secrets will never end up in your version control software.
