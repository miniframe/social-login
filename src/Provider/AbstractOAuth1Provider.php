<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\Core\Config;
use Miniframe\Core\Registry;
use Miniframe\Core\Request;
use Miniframe\Middleware\Session;
use Miniframe\Response\RedirectResponse;
use Miniframe\SocialLogin\Middleware\SocialLogin;
use Miniframe\SocialLogin\Model\User;

abstract class AbstractOAuth1Provider extends AbstractOAuthProvider
{
    /**
     * Returns the request token URL
     *
     * @return string
     */
    abstract protected function getRequestTokenUrl(): string;

    /**
     * Returns the authorize URL
     *
     * @return string
     */
    abstract protected function getAuthorizeUrl(): string;

    /**
     * Returns the access token URL
     *
     * @return string
     */
    abstract protected function getAccessTokenUrl(): string;
    /**
     * Returns the user profile
     *
     * @param array $accessToken The access token.
     *
     * @return User
     */
    abstract protected function getUserProfile(array $accessToken): User;

    /**
     * The OAuth consumer key
     *
     * @var string
     */
    protected $oauthConsumerKey;
    /**
     * The OAuth consumer secret
     *
     * @var mixed
     */
    protected $oauthConsumerSecret;
    /**
     * Reference to the SocialLogin middleware
     *
     * @var SocialLogin
     */
    protected $socialLogin;

    /**
     * Reference to the Session object.
     *
     * @var Session
     */
    protected $session;

    /**
     * Creates a new OAuth 1 provider
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);

        // Get config
        $this->oauthConsumerKey = $config->get('sociallogin-' . $this->shortName, 'consumer_key');
        $this->oauthConsumerSecret = $config->get('sociallogin-' . $this->shortName, 'consumer_secret');

        // Fetch some required middlewares
        $this->session = Registry::get(Session::class);
        $this->socialLogin = Registry::get(SocialLogin::class);
    }

    /**
     * Generates a signature
     *
     * @param string      $baseUrl           The base request URL.
     * @param string      $requestMethod     GET or POST.
     * @param array       $requestParameters The request parameters.
     * @param string|null $accessTokenSecret The access token secret (optional).
     *
     * @see https://developer.twitter.com/en/docs/authentication/oauth-1-0a/creating-a-signature
     *
     * @return string
     */
    protected function generateOAuthHeader(
        string $baseUrl,
        string $requestMethod = 'POST',
        array $requestParameters = array(),
        string $accessTokenSecret = null
    ): string {
        $oauthParameters = [
            'oauth_nonce' => md5(time() . __FILE__ . rand(1000, 9999)),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => time(),
            'oauth_consumer_key' => $this->oauthConsumerKey,
            'oauth_version' => '1.0',
        ];

        foreach ($requestParameters as $key => $value) {
            if (substr($key, 0, 6) !== 'oauth_') {
                continue;
            }
            $oauthParameters[$key] = $value;
        }

        // Take the request parameters and add OAuth parameters
        $parameters = array_merge($oauthParameters, $requestParameters);

        // Sort by key
        ksort($parameters);

        // Create parameter string
        $parameterString = '';
        foreach ($parameters as $key => $value) {
            $parameterString .= '&' . rawurlencode($key) . '=' . rawurlencode($value);
        }

        // Create base string and signing key
        $signatureBaseString = strtoupper($requestMethod) . '&' . rawurlencode($baseUrl) . '&'
            . rawurlencode(substr($parameterString, 1));
        $signingKey = rawurlencode($this->oauthConsumerSecret) . '&' . rawurlencode($accessTokenSecret ?? '');

        // Add signature to parameters
        $oauthParameters['oauth_signature'] = base64_encode(hash_hmac('sha1', $signatureBaseString, $signingKey, true));

        $header = '';
        foreach ($oauthParameters as $key => $value) {
            $header .= ', ' . rawurlencode($key) . '="' . rawurlencode($value) . '"';
        }

        return 'Authorization: OAuth ' . substr($header, 2);
    }

    /**
     * Fetches the provider state from the session.
     *
     * @return array
     */
    protected function getProviderState(): array
    {
        $session = $this->session->get('_SOCIALLOGIN');
        return $session[get_called_class()] ?? [];
    }

    /**
     * Updates the provider state in the session.
     *
     * @param array $state The provider state.
     *
     * @return void
     */
    protected function setProviderState(array $state): void
    {
        $session = $this->session->get('_SOCIALLOGIN');
        $session[get_called_class()] = $state;
        $this->session->set('_SOCIALLOGIN', $session);
    }

    /**
     * Starts the authentication process
     *
     * @return User
     */
    public function authenticate(): User
    {
        // Fetch the provider state
        $providerState = $this->getProviderState();

        // Store the user state, since redirecting with OAuth 1.0 will result in losing this state.
        if ($this->request->getRequest('state')) {
            $providerState['userState'] = $this->request->getRequest('state');
        } elseif ($this->request->getPost('state')) {
            $providerState['userState'] = $this->request->getPost('state');
        }
        $this->setProviderState($providerState);

        // Returned after authorization, does the token match with the one saved in the providerState?
        if (
            isset($providerState['requestOAuthToken'])
            && $providerState['requestOAuthToken'] === $this->request->getRequest('oauth_token')
            && $this->request->getRequest('oauth_verifier')
        ) {
            // Resets the state
            unset($providerState['requestOAuthToken']);
            unset($providerState['requestOAuthTokenSecret']);
            $this->setProviderState($providerState);

            // Requesting an access token, fetches the user profile and logs in
            $accessToken = $this->requestAccessToken();
            $user = $this->getUserProfile($accessToken);

            // When a redirect URL is in the state, we need to manually login and force a redirect since the state has
            // been lost in the OAuth redirect.
            $userState = SocialLogin::parseState($providerState['userState'] ?? null);
            if (empty($userState['redirectUrl'])) {
                $this->setProviderState([]);
                return $user;
            } else {
                $redirectUrl = $userState['redirectUrl'] ?? $this->baseHref;
                $providerState['userState'] = null;
                $this->setProviderState([]);
                $this->socialLogin->login($user);
                throw new RedirectResponse($redirectUrl);
            }
        }

        // No valid request token known at this point.
        // Requesting a request token
        $token = $this->requestRequestToken();

        // Store request token in state
        $providerState['requestOAuthToken'] = $token['oauth_token'];
        $providerState['requestOAuthTokenSecret'] = $token['oauth_token_secret'];
        $this->setProviderState($providerState);

        // Redirect to authorize URL
        $authorizeUrl = $this->getAuthorizeUrl();
        $authorizeUrl .= (strpos($authorizeUrl, '?') === false ? '?' : '&');
        $authorizeUrl .= 'oauth_token=' . rawurlencode($token['oauth_token']);
        throw new RedirectResponse($authorizeUrl);
    }

    /**
     * Requests a Request token and returns an array with keys 'oauth_token' and 'oauth_token_secret'
     *
     * @return string[]
     */
    protected function requestRequestToken(): array
    {
        // Compiles the request
        $data = ['oauth_callback' => $this->getCurrentUri()];
        $requestMethod = 'POST';
        $url = $this->getRequestTokenUrl();
        $headers = [$this->generateOAuthHeader($url, 'POST', $data)];

        // Requests the token
        $result = $this->curlRequest($url, $requestMethod, $data, $headers);
        // Validates the result
        foreach (['oauth_token', 'oauth_token_secret'] as $key) {
            if (!isset($result[$key])) {
                throw new \RuntimeException('Request Token failed: No ' . $key . ' returned');
            }
        }
        return $result;
    }

    /**
     * Request an Access token and returns an array with keys 'oauth_token' and 'oauth_token_secret'
     *
     * @return string[]
     */
    protected function requestAccessToken(): array
    {
        // Compiles the request
        $data = [
            'oauth_token' => $this->request->getRequest('oauth_token'),
            'oauth_verifier' => $this->request->getRequest('oauth_verifier')
        ];
        $requestMethod = 'POST';
        $url = $this->getAccessTokenUrl();
        $headers = [$this->generateOAuthHeader($url, 'POST', $data)];

        // Requests the token
        $result = $this->curlRequest($url, $requestMethod, $data, $headers);
        // Validates the result
        foreach (['oauth_token', 'oauth_token_secret'] as $key) {
            if (!isset($result[$key])) {
                throw new \RuntimeException('Request Token failed: No ' . $key . ' returned');
            }
        }
        return $result;
    }
}
