<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\SocialLogin\Model\User;

class Bitbucket extends AbstractOAuth2Provider
{
    /**
     * Returns the Authorize URL
     *
     * @return string
     */
    protected function getAuthorizeUrl(): string
    {
        return 'https://bitbucket.org/site/oauth2/authorize';
    }

    /**
     * Returns the Access Token URL
     *
     * @return string
     */
    protected function getAccessTokenUrl(): string
    {
        return 'https://bitbucket.org/site/oauth2/access_token';
    }

    /**
     * Returns the requested scope
     *
     * @return string|null
     */
    protected function getScope(): ?string
    {
        return 'account';
    }

    /**
     * Returns the user profile
     *
     * @param array $accessToken The access token.
     *
     * @return User
     */
    protected function getUserProfile(array $accessToken): User
    {
        $data = $this->curlRequest('https://api.bitbucket.org/2.0/user', 'GET', null, [
            'Authorization: Bearer ' . $accessToken['access_token'],
        ]);
        if ($data['account_status'] !== 'active') {
            throw new \RuntimeException('User is inactive');
        }

        return new User(
            $data['uuid'],
            $data['username'],
            $data['display_name'],
            $data['links']['avatar']['href'],
            static::class,
            ['accessToken' => $accessToken, 'userData' => $data]
        );
    }

    /**
     * Returns the image source for the logo of this provider.
     *
     * @return string
     */
    public static function getLogoSource(): string
    {
        return 'data:image/svg+xml;base64,'
            . base64_encode(file_get_contents(__DIR__ . '/../../templates/logos/Bitbucket.svg'));
    }

    /**
     * Returns the theme color for this provider.
     *
     * @return string
     */
    public static function getThemeColor(): string
    {
        return 'rgb(8,76,164)';
    }
}
