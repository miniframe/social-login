# Github OAuth 2.0 provider

This provider is written to support Github logins. For Githubs documentation, see
https://docs.github.com/en/developers/apps/building-oauth-apps/authorizing-oauth-apps

## How to configure this provider

For OAuth 2.0 providers, you'll need a client ID and client secret.
To get them, you need to register an app at the login provider.

For Github, you can do that by following these steps:

* In the upper-right corner of any page, click your profile photo, then click `Settings`.
* In the left sidebar, click `Developer settings`.
* Now click `OAuth Apps` and click `New OAuth App`.
* Fill this page in as good as possible. For the `Authorization callback URL`, fill in `https://{your-domain-here}/_SOCIALLOGIN/login/github`
* Click `Register application`
* Now you'll see a `Client ID`. Click on `Generate new client secret` to also see a client secret.
* Put this in your `sociallogin.ini` file:
  ```ini
  [sociallogin]
  providers[] = Github

  [sociallogin-github]
  client_id = ********************
  ```  
  And this in your `secrets.ini` file:
  ```ini
  [sociallogin-github]
  client_secret = ****************************************
  ```  
  It's recommended to add the secrets.ini file in your `.gitignore` file, so secrets will never end up in your version control software.
