<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\SocialLogin\Model\User;

class Gitlab extends AbstractOAuth2Provider
{
    /**
     * Returns the Authorize URL
     *
     * @return string
     */
    protected function getAuthorizeUrl(): string
    {
        return 'https://gitlab.com/oauth/authorize';
    }

    /**
     * Returns the Access Token URL
     *
     * @return string
     */
    protected function getAccessTokenUrl(): string
    {
        return 'https://gitlab.com/oauth/token';
    }

    /**
     * Returns the requested scope
     *
     * @return string|null
     */
    protected function getScope(): ?string
    {
        return 'read_user';
    }

    /**
     * Returns the user profile
     *
     * @param array $accessToken The access token.
     *
     * @return User
     */
    protected function getUserProfile(array $accessToken): User
    {
        $data = $this->curlRequest('https://gitlab.com/api/v4/user', 'GET', null, [
            'Authorization: Bearer ' . $accessToken['access_token'],
        ]);

        return new User(
            $data['id'],
            $data['username'],
            $data['name'],
            $data['avatar_url'],
            static::class,
            ['accessToken' => $accessToken, 'userData' => $data]
        );
    }

    /**
     * Returns the image source for the logo of this provider.
     *
     * @return string
     */
    public static function getLogoSource(): string
    {
        return 'data:image/svg+xml;base64,'
            . base64_encode(file_get_contents(__DIR__ . '/../../templates/logos/Gitlab.svg'));
    }

    /**
     * Returns the theme color for this provider.
     *
     * @return string
     */
    public static function getThemeColor(): string
    {
        return 'rgba(51,46,107,255)';
    }
}
