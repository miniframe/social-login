<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\SocialLogin\Model\User;

class Reddit extends AbstractOAuth2Provider
{
    /**
     * User agent for the cURL requests (Reddit requires a specific format)
     *
     * @var string
     */
    protected $userAgent = 'PHP:MiniframeSocialLogin:1.0 (by /u/garrcomm)';

    /**
     * Returns the Authorize URL
     *
     * @return string
     */
    protected function getAuthorizeUrl(): string
    {
        return 'https://www.reddit.com/api/v1/authorize';
    }

    /**
     * Returns the Access Token URL
     *
     * @return string
     */
    protected function getAccessTokenUrl(): string
    {
        return 'https://www.reddit.com/api/v1/access_token';
    }

    /**
     * Returns the requested scope
     *
     * @return string|null
     */
    protected function getScope(): ?string
    {
        return 'identity';
    }

    /**
     * Gets a full authorize URL, including the client ID and all other required parameters.
     *
     * @param array $data Optionally extra fields to append to the authorize URL.
     *
     * @return string
     */
    protected function getFullAuthorizeUrl(array $data = array()): string
    {
        $data['duration'] = 'temporary';
        return parent::getFullAuthorizeUrl($data);
    }

    /**
     * Reddit doesn't follow the original flow but has another way instead for getting the access token.
     *
     * @param string $code The provided code.
     *
     * @return array
     */
    protected function getAccessToken(string $code): array
    {
        $data = array(
            'code' => $code,
            'redirect_uri' => $this->getCurrentUri(),
            'grant_type' => 'authorization_code',
        );
        $headers = array(
            'Authorization: Basic ' . base64_encode($this->clientId . ':' . $this->clientSecret),
        );

        return $this->curlRequest($this->getAccessTokenUrl(), 'POST', $data, $headers);
    }

    /**
     * Returns the user profile
     *
     * @param array $accessToken The access token.
     *
     * @return User
     */
    protected function getUserProfile(array $accessToken): User
    {
        $data = $this->curlRequest('https://oauth.reddit.com/api/v1/me', 'GET', null, [
            'Authorization: Bearer ' . $accessToken['access_token'],
        ]);

        if ($data['subreddit']['user_is_banned']) {
            throw new \RuntimeException('User is banned');
        }
        if (!$data['has_verified_email']) {
            throw new \RuntimeException('Email address not yet verified');
        }

        return new User(
            $data['id'],
            $data['name'],
            $data['subreddit']['title'],
            html_entity_decode($data['icon_img']), // Reddit encodes the image URL for some reason
            static::class,
            ['accessToken' => $accessToken, 'userData' => $data]
        );
    }

    /**
     * Returns the image source for the logo of this provider.
     *
     * @return string
     */
    public static function getLogoSource(): string
    {
        return 'data:image/svg+xml;base64,'
            . base64_encode(file_get_contents(__DIR__ . '/../../templates/logos/Reddit.svg'));
    }

    /**
     * Returns the theme color for this provider.
     *
     * @return string
     */
    public static function getThemeColor(): string
    {
        return 'rgb(255,99,20)';
    }
}
