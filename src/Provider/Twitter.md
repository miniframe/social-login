# Twitter OAuth 1.0 provider

This provider is written to support Twitter logins. For Twitters documentation, see
https://developer.twitter.com/en/docs/authentication/oauth-1-0a/obtaining-user-access-tokens

Create app @ https://developer.twitter.com/en/portal/dashboard

## How to configure this provider

For OAuth 1.0 providers, you'll need a consumer key and consumer secret.
To get them, you need to register an app at the login provider.

For Twitter, you can do that at https://developer.twitter.com/en/portal/dashboard
Creating an app will result in an API key and API key secret.
These will be used as consumer key and consumer key secret.

Take the `API Key` and use it as `consumer_key` in your .ini file:
```ini
[sociallogin]
providers[] = Twitter

[sociallogin-twitter]
consumer_key = *************************
```

Take the `API Key Secret` and use it as `consumer_key_secret` in your `secrets.ini`:
```ini
[sociallogin-twitter]
consumer_secret = **************************************************
```
It's recommended to add the secrets.ini file in your `.gitignore` file, so secrets will never end up in your version control software.
