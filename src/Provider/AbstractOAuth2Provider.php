<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Response\RedirectResponse;
use Miniframe\SocialLogin\Middleware\SocialLogin;
use Miniframe\SocialLogin\Model\User;

abstract class AbstractOAuth2Provider extends AbstractOAuthProvider
{
    /**
     * Returns the Authorize URL
     *
     * @return string
     */
    abstract protected function getAuthorizeUrl(): string;

    /**
     * Returns the Access Token URL
     *
     * @return string
     */
    abstract protected function getAccessTokenUrl(): string;

    /**
     * Returns the requested scope
     *
     * @return string|null
     */
    abstract protected function getScope(): ?string;

    /**
     * Returns the user profile
     *
     * @param array $accessToken The access token.
     *
     * @return User
     */
    abstract protected function getUserProfile(array $accessToken): User;

    /**
     * OAuth 2.0 Client ID
     *
     * @var string
     */
    protected $clientId;

    /**
     * OAuth 2.0 Client Secret
     *
     * @var string
     */
    protected $clientSecret;

    /**
     * Creates a new OAuth 2.0 provider
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);

        // Get config
        $this->clientId = $config->get('sociallogin-' . $this->shortName, 'client_id');
        $this->clientSecret = $config->get('sociallogin-' . $this->shortName, 'client_secret');
    }

    /**
     * Starts the authentication process
     *
     * @return User
     */
    public function authenticate(): User
    {
        if ($this->request->getRequest('state') && $this->request->getRequest('code')) {
            $state = SocialLogin::parseState($this->request->getRequest('state'));
            if (!isset($state['redirectUrl'])) {
                throw new \RuntimeException('State incomplete');
            }

            $accessToken = $this->getAccessToken($this->request->getRequest('code'));
            if (!isset($accessToken['access_token'])) {
                throw new \RuntimeException('Did\'t get an access token');
            }
            return $this->getUserProfile($accessToken);
        }

        throw new RedirectResponse($this->getFullAuthorizeUrl());
    }

    /**
     * Requests the access token based on a code from the OAuth provider
     *
     * @param string $code The provided code.
     *
     * @return array
     */
    protected function getAccessToken(string $code): array
    {
        $data = array(
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'code' => $code,
            'redirect_uri' => $this->getCurrentUri(),
            'grant_type' => 'authorization_code',
        );

        return $this->curlRequest($this->getAccessTokenUrl(), 'POST', $data);
    }

    /**
     * Gets a full authorize URL, including the client ID and all other required parameters.
     *
     * @param array $data Optionally extra fields to append to the authorize URL.
     *
     * @return string
     */
    protected function getFullAuthorizeUrl(array $data = array()): string
    {
        $data['client_id'] = $this->clientId;
        $data['redirect_uri'] = $this->getCurrentUri();
        $data['state'] = $this->generateState();
        $data['response_type'] = 'code';
        if ($this->getScope()) {
            $data['scope'] = $this->getScope();
        }

        return $this->getAuthorizeUrl()
            . (strpos($this->getAuthorizeUrl(), '?') !== false ? '&' : '?')
            . http_build_query($data);
    }

    /**
     * Generates the state string
     *
     * @return string
     */
    protected function generateState(): string
    {
        $state = array();
        if ($this->request->getRequest('state')) {
            $state = SocialLogin::parseState($this->request->getRequest('state'));
        }
        if (!isset($state['redirectUrl'])) {
            $state['redirectUrl'] = $this->baseHref;
        }
        return SocialLogin::generateState($state);
    }
}
