<?php

namespace Miniframe\SocialLogin\Provider;

use Miniframe\Core\Config;
use Miniframe\Core\Registry;
use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Mailer\Middleware\Mailer;
use Miniframe\Mailer\Model\Recipient;
use Miniframe\Middleware\Session;
use Miniframe\Response\RedirectResponse;
use Miniframe\SocialLogin\Middleware\SocialLogin;
use Miniframe\SocialLogin\Model\User;

class Email implements ProviderInterface
{
    /**
     * Reference to the Request object.
     *
     * @var Request
     */
    protected $request;
    /**
     * Reference to the Config object.
     *
     * @var Config
     */
    protected $config;
    /**
     * Reference to the Session object.
     *
     * @var Session
     */
    protected $session;
    /**
     * The base URL for the social login page.
     *
     * @var string
     */
    protected $socialLoginPrefixUrl;

    /**
     * Initializes the Email service
     *
     * @param Request      $request              Reference to the Request object.
     * @param Config       $config               Reference to the Config object.
     * @param Session|null $session              Reference to the user session.
     * @param string|null  $socialLoginPrefixUrl The base URL for the social login page.
     */
    public function __construct(
        Request $request,
        Config $config,
        Session $session = null,
        string $socialLoginPrefixUrl = null
    ) {
        // Parameters are required, but can't be defined as required by its interface
        if ($session === null || $socialLoginPrefixUrl === null) {
            throw new \BadFunctionCallException(
                'The Email provider works differently, to enable this provider, see '
                . 'https://bitbucket.org/miniframe/miniframe-social-login/src/v1/src/Provider/Email.md'
            );
        }

        $this->request = $request;
        $this->config = $config;
        $this->session = $session;
        $this->socialLoginPrefixUrl = $socialLoginPrefixUrl;
    }

    /**
     * Starts the authentication process
     *
     * @return User
     */
    public function authenticate(): User
    {
        $state = SocialLogin::parseState($this->request->getRequest('state') ?? $this->request->getPost('state'));

        // Validate code when specified
        if ($this->request->getPost('code') !== null) {
            // Validate the code and return the User object
            return $this->getUserByEmail($this->validateCode($this->request->getPost('code')));
        }

        // When no code is specified, send email with code
        if ($this->request->getPost('email') !== null) {
            $this->sendCode($this->request->getPost('email'));


            // Redirect to Code page
            throw new RedirectResponse(
                $this->socialLoginPrefixUrl . 'email/code?state=' . rawurlencode(SocialLogin::generateState($state))
            );
        }

        // No email address, nor a code specified
        throw new RedirectResponse(
            $this->socialLoginPrefixUrl . 'login?state=' . rawurlencode(SocialLogin::generateState($state))
        );
    }

    /**
     * Returns the image source for the logo of this provider.
     *
     * @return string
     */
    public static function getLogoSource(): string
    {
        return 'data:image/svg+xml;base64,'
            . base64_encode(file_get_contents(__DIR__ . '/../../templates/logos/Email.svg'));
    }

    /**
     * Returns the theme color for this provider.
     *
     * @return string
     */
    public static function getThemeColor(): string
    {
        return '#c0c0c0';
    }

    /**
     * Sends a code to a specific mail address
     *
     * @param string $email The mail address.
     *
     * @return void
     */
    protected function sendCode(string $email): void
    {
        // Validate email address
        if (!trim($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException('Invalid email specified');
        }

        // Modify session
        $code = $this->generateCode();
        $session = $this->session->get('_SOCIALLOGIN');
        $session['email']['address'] = $email;
        $session['email']['expires'] = time() + (10 * 60);
        $session['email']['code'] = $code;
        $this->session->set('_SOCIALLOGIN', $session);

        if (Registry::has(Mailer::class)) {
            // Send the mail with the Mailer middleware
            $mailer = 'sendCodeMailer';
        } else {
            // Send the mail with the internal mailer
            $mailer = 'sendCodeInternal';
        }
        $this->$mailer(
            $session['email']['code'],
            $email,
            $this->config->get('sociallogin-email', 'from_name'),
            $this->config->get('sociallogin-email', 'from_email')
        );
    }

    /**
     * Send the code with the Mailer middleware
     *
     * @param string $code      The code.
     * @param string $email     Recipient.
     * @param string $fromName  Sender name.
     * @param string $fromEmail Sender email.
     *
     * @return void
     */
    protected function sendCodeMailer(string $code, string $email, string $fromName, string $fromEmail): void
    {
        $mailer = Registry::get(Mailer::class);
        $mailer->sendMail(
            new Recipient($email),
            'Your code to login',
            new Response('Your code is ' . $code . "\r\n"),
            false,
            null,
            [
                'from_name' => $fromName,
                'from_email' => $fromEmail,
            ]
        );
    }

    /**
     * Send the code with the internal mail() function
     *
     * @param string $code      The code.
     * @param string $email     Recipient.
     * @param string $fromName  Sender name.
     * @param string $fromEmail Sender email.
     *
     * @return void
     */
    protected function sendCodeInternal(string $code, string $email, string $fromName, string $fromEmail): void
    {
       // Define some variables
        $recipient = '"' . ucfirst(explode('@', $email)[0]) . '" <' . $email . '>';
        $sender = '"' . $fromName . '" <' . $fromEmail . '>';

        // Set up mail
        $headers = [
            'From: ' . $sender,
            'Reply-To: ' . $sender,
            'To: ' . $recipient,
            'X-Mailer: Miniframe Social Login',
        ];
        $body = 'Your code is ' . $code . "\r\n";

        if (
            !mail(
                $recipient,
                'Your code to login',
                $body,
                implode("\r\n", $headers)
            )
        ) {
            throw new \RuntimeException('Couldn\t send email.');
        }
    }

    /**
     * Validates the code and returns the email address.
     *
     * @param string $code The code.
     *
     * @return string
     */
    protected function validateCode(string $code): string
    {
        // Validate session
        $session = $this->session->get('_SOCIALLOGIN');
        if (!isset($session['email']) || !isset($session['email']['code']) || !isset($session['email']['expires'])) {
            throw new \InvalidArgumentException('No session available. Please try again.');
        }
        // Validate expiration
        if ($session['email']['expires'] < time()) {
            throw new \InvalidArgumentException('Session expired. Please try again.');
        }
        // Validate code
        if (strtoupper($session['email']['code']) != strtoupper(trim($code))) {
            throw new \InvalidArgumentException('Invalid code. Please try again.');
        }
        // Destroy session
        $return = $session['email']['address'];
        unset($session['email']);
        $this->session->set('_SOCIALLOGIN', $session);

        return $return;
    }

    /**
     * Creates a User object based on an email address.
     *
     * @param string $email The mail address.
     *
     * @return User
     */
    protected function getUserByEmail(string $email): User
    {
        return new User(
            $email,
            $email,
            ucfirst(explode('@', $email)[0]),
            'https://s.gravatar.com/avatar/' . md5(strtolower(trim($email))) . '?s=80&d=identicon',
            static::class,
            array()
        );
    }

    /**
     * Generates a random code
     *
     * @param integer $digits Length of the code.
     * @param string  $chars  The characters in the code (1 and I are removed because they look similar).
     *
     * @return string
     */
    protected function generateCode(int $digits = 6, string $chars = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ'): string
    {
        $return = '';
        for ($iterator = 0; $iterator < $digits; ++$iterator) {
            $return .= substr($chars, rand(0, strlen($chars) - 1), 1);
        }
        return $return;
    }
}
